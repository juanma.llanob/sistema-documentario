@extends('admin.layouts.app')

@section('title','Home')

@section('header')


@endsection

@section('home')
active
@endsection

@section('content')
	
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-9 main-chart">
        <!--CUSTOM CHART START -->
        <div class="border-head">
          <h3>Gastos Anual</h3>
        </div>
        <div class="custom-bar-chart ">
          <ul class="y-axis">
            <li><span>100.000</span></li>
            <li><span>80.000</span></li>
            <li><span>60.000</span></li>
            <li><span>40.000</span></li>
            <li><span>20.000</span></li>
            <li><span>0</span></li>
          </ul>
          <div class="bar">
            <div class="title">ENE</div>
            <div class="value tooltips" data-original-title="8.500" data-toggle="tooltip" data-placement="top">8.5%</div>
          </div>
          <div class="bar ">
            <div class="title">FEB</div>
            <div class="value tooltips" data-original-title="5.000" data-toggle="tooltip" data-placement="top">5.0%</div>
          </div>
          <div class="bar ">
            <div class="title">MAR</div>
            <div class="value tooltips" data-original-title="6.000" data-toggle="tooltip" data-placement="top">6.0%</div>
          </div>
          <div class="bar ">
            <div class="title">ABR</div>
            <div class="value tooltips" data-original-title="4.500" data-toggle="tooltip" data-placement="top">4.5%</div>
          </div>
          <div class="bar">
            <div class="title">MAY</div>
            <div class="value tooltips" data-original-title="3.200" data-toggle="tooltip" data-placement="top">3.2%</div>
          </div>
          <div class="bar ">
            <div class="title">JUN</div>
            <div class="value tooltips" data-original-title="6.200" data-toggle="tooltip" data-placement="top">6.2%</div>
          </div>
          <div class="bar ">
            <div class="title">JUL</div>
            <div class="value tooltips" data-original-title="6.200" data-toggle="tooltip" data-placement="top">6.2%</div>
          </div>
          <div class="bar">
            <div class="title">AGO</div>
            <div class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top">7.5%</div>
          </div>
          <div class="bar">
            <div class="title">SET</div>
            <div class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top">7.5%</div>
          </div>
          <div class="bar">
            <div class="title">OCT</div>
            <div class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top">7.5%</div>
          </div>
          <div class="bar">
            <div class="title">NOV</div>
            <div class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top">54%</div>
          </div>
          <div class="bar">
            <div class="title">DIC</div>
            <div class="value tooltips" data-original-title="7.500" data-toggle="tooltip" data-placement="top">75%</div>
          </div>
        </div>
        <div class="border-head">
        	
         	<h3 class=" mt-4 ">Documentos Técnicos <a class="btn btn-theme py-auto float-right " href="{{ route('admin.technical.index')}}">Ver todo >> </a></h3>
         	

        </div>
        <table class="table">
          <thead>
            <tr>
              <th style="width:60px" class="text-center">Nº</th>
              <th class="text-left">Nombre</th>
              <th style="width:140px" class="text-right">Fecha</th>
              <th style="width:90px" class="">Documento</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-center">1</td>
              <td><a class="link-theme" href="#">Documento 1</a></td>
              <td class="text-right">14-32-2019</td>
              <td class="text-center"><i class="fa fa-file-pdf-o"></i></td>
            </tr>
            <tr>
              
            </tr>
            <tr>
              <td class="text-center">2</td>
              <td><a class="link-theme" href="#">Documento 1</a></td>
              <td class="text-right">14-32-2019</td>
              <td class="text-center"><i class="fa fa-file-pdf-o"></i></td>
            </tr>
            <tr>
              <td class="text-center">3</td>
              <td><a class="link-theme" href="#">Documento 1</a></td>
              <td class="text-right">14-32-2019</td>
              <td class="text-center"><i class="fa fa-file-pdf-o"></i></td>
            </tr>
            <tr>
              <td class="text-center">4</td>
              <td><a class="link-theme" href="#">Documento 1</a></td>
              <td class="text-right">14-32-2019</td>
              <td class="text-center"><i class="fa fa-file-pdf-o"></i></td>
            </tr>
            <tr>
              <td class="text-center">5</td>
              <td><a class="link-theme" href="#">Documento 1</a></td>
              <td class="text-right">14-32-2019</td>
              <td class="text-center"><i class="fa fa-file-pdf-o"></i></td>
            </tr>
          </tbody>
        </table>


   	  </div>	

      <div class="col-lg-3 ds">
        <!--COMPLETED ACTIONS DONUTS CHART-->
        <div class="donut-main">
          <h4>DOCUMENTOS FINANCIERO PROGRESO</h4>
          <canvas id="newchart" height="130" width="130"></canvas>
          <script>
            var doughnutData = [{
                value: 70,
                color: "#4ECDC4"
              },
              {
                value: 30,
                color: "#fdfdfd"
              }
            ];
            var myDoughnut = new Chart(document.getElementById("newchart").getContext("2d")).Doughnut(doughnutData);
          </script>
        </div>
        <!--NEW EARNING STATS -->
        <div class="panel terques-chart">
          <div class="panel-body">
            <div class="chart">
              <div class="centered">
                <span>TOTAL HASTA HOY</span>
                <strong>$ 890,00 | 15%</strong>
              </div>
            </div>
          </div>
        </div>
        <!--new earning end-->
        <!-- RECENT ACTIVITIES SECTION -->
        <h4 class="centered mt">ACTIVIDADES RECIENTES</h4>
        <!-- First Activity -->
        <div class="desc">
          <div class="thumb">
            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
          </div>
          <div class="details">
            <p>
              <muted>Just Now</muted>
              <br/>
              <a href="#">Paul Rudd</a> purchased an item.<br/>
            </p>
          </div>
        </div>
        <!-- Second Activity -->
        <div class="desc">
          <div class="thumb">
            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
          </div>
          <div class="details">
            <p>
              <muted>2 Minutes Ago</muted>
              <br/>
              <a href="#">James Brown</a> subscribed to your newsletter.<br/>
            </p>
          </div>
        </div>
        <!-- Third Activity -->
        <div class="desc">
          <div class="thumb">
            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
          </div>
          <div class="details">
            <p>
              <muted>3 Hours Ago</muted>
              <br/>
              <a href="#">Diana Kennedy</a> purchased a year subscription.<br/>
            </p>
          </div>
        </div>
        <!-- Fourth Activity -->
        <div class="desc">
          <div class="thumb">
            <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
          </div>
          <div class="details">
            <p>
              <muted>7 Hours Ago</muted>
              <br/>
              <a href="#">Brando Page</a> purchased a year subscription.<br/>
            </p>
          </div>
        </div>
        
        <!-- CALENDAR-->
        <div id="calendar" class="mb">
          <div class="panel green-panel no-margin">
            <div class="panel-body">
              <div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                <div class="arrow"></div>
                <h3 class="popover-title" style="disadding: none;"></h3>
                <div id="date-popover-content" class="popover-content"></div>
              </div>
              <div id="my-calendar"></div>
            </div>
          </div>
        </div>
        <!-- / calendar -->
      </div>
      <!-- /col-lg-3 -->
    </div>
    <!-- /row -->
  </section>
</section>


@endsection

@section('footer')


@endsection