@extends('admin.layouts.app')

@section('title','Documentos Técnicos')

@section('header')


@endsection
@section('document')
active
@endsection
@section('technical')
active
@endsection
@section('content')

	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{{ route('admin.technical.create')}}" class="btn btn-compose text-truncate">
                  <i class="fa fa-pencil"></i>  Agregar Documento
                </a>
                <ul class="nav nav-pills nav-stacked mail-nav row">
                   <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Sin revisar
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Aprobados
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Desaprobados
                  		<span class="label label-info pull-right inbox-notification">8</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Observados
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista de Documentos Técnicos
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($technicals as $technical)
          						<tr class="">
          							<td class="inbox-small-cells">
          								<input type="checkbox" class="mail-checkbox">
          							</td>
          							<td class="view-message "><a href="{{ route('admin.technical.show', $technical) }}">{{ $technical->name }}</a></td>
          							<td class="view-message "><a href="{{ route('admin.technical.show', $technical) }}">{{ $technical->production->name }}</a></td>
          							<td class="view-message ">
          								<a href="{{ route('admin.technical.show', $technical) }}">
          			           {{ $technical->status}}       		
          								</a>
          							</td>
          							<td class="view-message "><a href="{{ route('admin.technical.downloadPDF', $technical) }}"><i class="fa fa-file-pdf-o"></i></a></td>
          							<td class="view-message  ">
          								<a href="{{ route('admin.technical.show', $technical) }}" class="px-3">
          									{{ $technical->updated_at }}
          								</a>
          							</td>
                        <td class="view-message  text-right">
                          <a href="{{ route('admin.technical.edit', $technical) }}" class="px-3">
                            <i class="fa fa-pencil">
                              
                            </i>
                          </a>
                        </td>
          						</tr>
          						@endforeach
                    </tbody>
                  </table>
                  <div class="w-100 text-right">
                    {!! $technicals->links() !!}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>


	
@endsection

@section('footer')


@endsection