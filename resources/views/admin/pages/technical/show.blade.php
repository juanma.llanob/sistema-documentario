@extends('admin.layouts.app')

@section('title','Documento Técnico'.$technical->code)

@section('header')


@endsection
@section('document')
active
@endsection
@section('technical')
active
@endsection
@section('content')

		
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <div class="nav  ">

                  <div class="col-lg-10 col-md-9 col-8  my-3" >
                    <h4 class="text-truncate">Datos del Documento  <strong>{{ $technical->name }}</strong><h4/> 

                  </div>
                  <div class=" my-auto col-lg-2 col-md-3 col-4 ">
                  	<div class="dropdown  float-right">
          					  <button class="btn btn-theme dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          					    ACCIONES
          					  </button>
          					  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          					    <a class="dropdown-item" href="{{ route('admin.technical.edit',$technical)}}">Editar</a>
          					    <a class="dropdown-item" href="">Desactivar</a>
          					    <form method="POST" action="{{ route('admin.technical.destroy',$technical)}}">
          					    	@csrf
          	    					{{ method_field('DELETE') }}
          	    					<button class="dropdown-item text-danger" type="submit" >Eliminar</button>
          					    </form>

          					  </div>
          					</div>
                  </div>
                </div>
          
              </div>

              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        

                        	<div class="col-lg-6 col-md-12  ">

                        		<h3>Datos </h3>
                        		<table class="table  ">
                        			<tr class="">
		                                <td class="" style="width:130px">
		                                  Titulo :
		                                </td>
		                                <td class="">
		                                  {{ $technical->name }}
		                                </td>     
		                            </tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Código :
              							</td>
              							<td class="">
              								{{ $technical->code}}
              							</td>			
                					</tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Descripción :
              							</td>
              							<td class="">
              								{!! nl2br(e($technical->observation)) !!}
              							</td>			
                					</tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Tipo de Documento :
              							</td>
              							<td class="">
              								{{ $technical->technicaltype->name}}
              							</td>			
                					</tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Centro de Producción  :
              							</td>
              							<td class="">
              								{{ $technical->production->name}}
              							</td>			
                					</tr>

		                    							
                				</table>
                        	</div>
		                    <div class="col-lg-6 col-md-12  ">
		                    	<h3>Archivo</h3>
	                        	<a href="{{ route('admin.technical.downloadPDF',$technical) }}"><i class="fa fa-file-pdf-o">
	                          		Decargar Archivo 
	                          	</i></a>
	                       </div>
        				<!-- /col-lg-8 -->
                      </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->

                </div>
                <!-- /tab-content -->
                
              <div class="panel-heading container">
                <ul class="nav border-bottom ">

                    <h4> Mensajes<h4/>
                  </li>
                </ul>              <!-- /panel-heading -->
	              <div class="panel-body w-100">
	              	<p> Total de mensajes {{ $technicalconditions->count() }}</p>
			        <div class="chat-room mt">

			          <aside class="mid-side">
			          	
			          	@foreach($technicalconditions as $technicalcondition)
				            @if($technicalcondition->user->id===Auth::user()->id )
					            <div class="group-rom">
					              <div class="first-part odd">{{ $technicalcondition->user->name }}</div>
					              <div class="second-part">{{ $technicalcondition->message}}</div>
					              <div class="third-part">{{ $technicalcondition->created_at}}</div>
					            </div>
				            @else
					            <div class="group-rom">
					              <div class="first-part">{{ $technicalcondition->user->name }}</div>
					              <div class="second-part">{{ $technicalcondition->message}}</div>
					              <div class="third-part">{{ $technicalcondition->created_at}}</div>
					            </div>
				          	@endif  
			            @endforeach
			            
			          </aside>
			          
			        </div>
			        <div class="my-5">
			            @if($technical->status !== 'aprobado')
				            <form  <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.technicalcondition.store',$technical) }}"> 
				              @csrf
				              <div class="chat-txt">
				                <input type="text" name="message" class="form-control" required="" placeholder="ingrese mensaje">
				                 <input type="text" hidden="" name="technical_id" value="{{ $technical->id}}" >
				                 
				              </div>

				              <button class="btn btn-theme" type="submit">Enviar mensaje</button>
				          	</form>
				        	
			        <!-- page end-->                  
	              	</div>
	                <!-- /tab-content -->
	                <div class="col-md-12 row">
                    	@if($technical->status!== 'desaprobado')
                    	<form class="col-md-2 " action="{{ route('admin.technical.update',$technical )}}" method="POST">
		                	@csrf
		          	    	{{ method_field('PUT') }}
		          	    	<input type="text" hidden="" name="status" value="desaprobado">
		          	    	<button class="btn btn-danger" type="submit" >Desaprobado</button>
		                </form>
		                @endif
	                	<form class="col-md-2 " action="{{ route('admin.technical.update',$technical )}}" method="POST">
		                	@csrf
		          	    	{{ method_field('PUT') }}
		          	    	<input type="text" hidden="" name="status" value="aprobado">
		          	    	<button class="btn btn-theme px-5" type="submit" >Aprobar</button>
		                </form>
                		@endif  
	            </div>
	        </div>
        </div>
            <!-- /col-lg-12 -->
    </div>
          

  </section>


</section>



@endsection

@section('footer')


@endsection