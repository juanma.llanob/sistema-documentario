@extends('admin.layouts.app')

@section('title','Editar Documento Técnico'.$technical->code)

@section('header')


@endsection
@section('document')
active
@endsection
@section('technical')
active
@endsection
@section('content')

		
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">

                  <li class="">
                    <h4 >Editar Documento Técnico {{ $technical->code}} <h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.technical.update',$technical) }}" enctype="multipart/form-data" >
                        	@csrf
                        	{{ method_field('PUT') }}
                        	<div class="col-lg-6 col-md-12  ">

                        	  <div class="form-group">
	                            <label class="col-12"> Título</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $technical->name }}"  autocomplete="name" autofocus placeholder=""> 
									@error('name')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12"> Código</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ $technical->code }}"  autocomplete="code" autofocus placeholder=""> 
									@error('code')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12"> Observación</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('observation') is-invalid @enderror" name="observation" value="{{ $technical->observation }}"  autocomplete="observation" autofocus placeholder=""> 
									@error('observation')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          
	                          <div class="form-group">
	                            <label class="col-12">Centro de Producción</label>
	                            <div class="col-12">
	                              
	                              	<select name="production_id" class="form-control @error('production_id') is-invalid @enderror" required>
										<option value="{{ $technical->production->id }}"  selected=""> {{ $technical->production->name }} </option>
										@foreach($productions as $production)
											@if($production->is_active($production) && $production->id!==1)
												<option value="{{ $production->id }}">{{ $production->name }}</option>
											@endif
										@endforeach
									</select>

									@error('production_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror

	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Tipo de Documento</label>
	                            <div class="col-12">
	                              
	                              	<select name="technicaltype_id" class="form-control @error('technicaltype_id') is-invalid @enderror" required>
										<option value="{{ $technical->technicaltype->id }} "  selected=""> {{ $technical->technicaltype->name }}  </option>
										@foreach($technicaltypes as $technicaltype)
												<option value="{{ $technicaltype->id }}">{{ $technicaltype->name }}</option>
										@endforeach
									</select>

									@error('technicaltype_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror

	                            </div>
	                          </div>
	                          <div class="form-group">
	                          	<label class="col-12">Documento </label>
	                          	<div class="col-12">
	                          		<a href="{{ route('admin.technical.downloadPDF',$technical) }}"><i class="fa fa-file-pdf-o">
	                          			{{ $technical->document}}
	                          		</i></a>
	                          	</div>
	                          </div>
	                          <div class="form-group">
	                          	<label class="col-12">Cambiar documento</label>
	                          	<div class="col-12">
	                          		<input type="file" name="document">
	                          	</div>
	                          </div>
	                          
	                          
	                        </div>
	                        
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Guardar</button>
	                              <a class="btn btn-theme04" href="{{  URL::previous() }}">Cancel</a>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>

    </section>
</section>


@endsection

@section('footer')


@endsection