@extends('admin.layouts.app')

@section('title',$production->name)

@section('header')


@endsection
@section('production')
active
@endsection
@section('content')
<section id="main-content">
      <section class="wrapper site-min-height">
      	<div class="row mt">


      		<div class="col-lg-4 col-md-12 col-12">
                <!-- WHITE PANEL - TOP USER -->
                <div class="panel ">
                  <div class=" chat-room-head panel w-100">
                    <h4 class="text-center">CENTRO DE PRODUCCIÓN {{ $production->name}}</h4>
                  </div>
                  <p class="text-center"><img src="{{ Storage::url($production->src) }}" class="img-circle " width="150"></p>
                  <div class="row">
                    <div class="col-12 row">
                      <p class=" col-4 text-right">CÓDIGO</p>
                      <p class="col-8 text-left">{{ $production->code }}</p>
                    </div>
                    <div class="col-12 row">
                      <p class=" col-4 text-right">CORREO</p>
                      <p class="col-8 text-left">{{ $production->email }}</p>
                    </div>
                    <div class="col-12 row">
                      <p class=" col-4 text-right">TELÉFONO</p>
                      <p class="col-8 text-left">{{ $production->phone }}</p>
                    </div>
                    <div class="col-12 row">
                      <p class=" col-4 text-right">ESTADO</p>
                      <p class="col-8 text-left ">
                      	@if($production->status===0)
                      		Eliminado
                      	@elseif($production->status===1)
                      		Activo
                      	@else
                      		Bloqueado
                      	@endif
                      </p>
                    </div>
                    <div class="col-12 row">
                      <p class=" col-4 text-right">DESCRIPCIÓN</p>
                      <p class="col-8 text-left">{{ $production->description }}</p>
                    </div>
                    <div class="col-12 row">
                      <p class=" col-4 text-right">CREADO</p>
                      <p class="col-8 text-left">{{ $production->created_at }}</p>
                    </div>

                  </div>
                  <div class="panel-body row justify-content-center">
                  	@if($production->id!==1 and $production->status===1)
              		<a href="{{ route('admin.production.edit', $production) }}" class="btn btn-theme02 text-truncate col-5 mx-2">
                  		<i class="fa fa-pencil"></i>  Editar</a>

                  	<form method="Post" action="{{ route('admin.production.destroy', $production) }}" class="w-50" >
						@csrf
					    {{ method_field('DELETE') }}
						<button type="submit" class="btn btn-danger">Bloquear</button>
					</form>
                  	
                	@elseif($production->id==1)
              		<a href="{{ route('admin.production.edit', $production) }}" class="btn btn-theme02 text-truncate col-5 mx-2">
                  		<i class="fa fa-pencil"></i>  Editar</a>
                	@else
					<a class="btn btn-theme01 col-5 mx-2"  href="{{ route('admin.production.update_all',$production) }}" >Activar</a>		
					
					@endif

              	  </div>
                </div>
              </div>

              <div class="col-lg-8 col-md-12 col-12">
	            <section class="panel">
	              <header class="panel-heading wht-bg">
	                <h4 class="gen-case">
	                    Lista de usuarios
	                    <form action="#" class="pull-right mail-src-position">
	                      <div class="input-append">
	                        <input type="text" class="form-control " placeholder="Search Mail">
	                      </div>
	                    </form>
	                  </h4>
	              </header>
	              <div class="panel-body minimal">
	         
	                <div class="table-inbox-wrap ">
	                  <table class="table table-inbox table-hover">
	                    <tbody>
	                    	@foreach($users as $user)
							<tr class="">
								<td class="inbox-small-cells">
									<input type="checkbox" class="mail-checkbox">
								</td>
								<td class="view-message  dont-show"><a href="{{ route('admin.user.show', $user) }}">{{ $user->surname.' '.$user->name}}</a></td>
								<td class="view-message ">
									<a href="{{ route('admin.user.show', $user) }}">
				                    	@if($user->status===0 )
				                    		<i class="fa fa-lock"></i> eliminado
				                    	@elseif($user->status==1)
				                    		<i class="fa fa-unlock"></i> activo
				                    	@else
											<i class="fa fa-unlock"></i> Bloqueado
				                    	@endif	
									</a>
								</td>
								<td class="view-message  text-right">
									<a href="{{ route('admin.user.edit', $user) }}" class="px-3"><i class="fa fa-pencil"></i></a>
									<a href="{{ route('admin.user.update', $user->id) }}" class="px-3"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							@endforeach
	                    </tbody>
	                  </table>
	                </div>
	              </div>
	            </section>
	          </div>

        </div>
    </section>


</section>


	
	<form method="Post" action="{{ route('admin.production.update', $production) }}" name="active">
		@csrf
	    {{ method_field('PUT') }}
	</form>
	
	
	
@endsection

@section('footer')


@endsection