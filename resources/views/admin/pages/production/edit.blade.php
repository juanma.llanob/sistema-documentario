@extends('admin.layouts.app')

@section('title','Editar'.$production->name)

@section('header')


@endsection
@section('production')
active
@endsection
@section('content')
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">

                  <li class="">
                    <h4 >Editar Centro : {{ $production->name }}<h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.production.update',$production) }}" enctype="multipart/form-data">
                        	@csrf
	                        {{ method_field('PUT') }}

                        	<div class="col-lg-6 col-md-12  ">


	                          <div class="form-group">
	                            <label class="col-12"> Nombre</label>
	                            <div class="col-12">
	                              	<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $production->name }}" required autocomplete="name" autofocus>

	                                @error('name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12"> Código</label>
	                            <div class="col-12">
	                              	<input id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ $production->code }}" required autocomplete="code" autofocus>

	                                @error('code')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12">Descripción</label>
	                            <div class="col-12">
	                              	<input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $production->description }}" required autocomplete="description" autofocus>

	                                @error('description')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12"> Correo</label>
	                            <div class="col-12">
	                              	<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $production->email }}" required autocomplete="email">

	                                @error('email')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Teléfono</label>
	                            <div class="col-12">
	                              	<input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $production->phone }}" required autocomplete="phone" autofocus>

	                                @error('phone')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          
	                          <div class="form-group">
	                            <label class="col-12">Estado</label>
	                            <div class="col-12">
	                              
	                              	<select name="status" class="form-control @error('status') is-invalid @enderror" required>
										<option value="{{ $production->status }}" selected=""> 
											@if($production->status===0)
												Eliminado </option>
												<option value="1"> Activo</option>
										 		<option value="2"> Bloqueado</option>
											@elseif($production->status ===1)
												Activo </option>
												<option value="0"> Eliminado</option>
												<option value="2"> Bloqueado</option>
											@else
												Bloqueado</option>
												<option value="0"> Eliminado</option>
										 		<option value="1"> Activo</option>
											@endif
										
									</select>

	                                @error('status')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          
	                          
	                        </div>
	                        <div class="col-lg-6 col-md-12  ">
	                        	

	                        	<div class="form-group ">

				                  <div class="col-12 text-center">
				                    <div class="fileupload fileupload-new " data-provides="fileupload">
				                      <div class="col-12 row">
				                      	<div class="fileupload-new thumbnail col-4 my-auto " >
					                        <img src="{{ Storage::url($production->src) }}"   />
					                    </div>
					                    <div class="col-4 my-auto">
											<p class="link-theme text-center"> Cambiar Imagen <i class="fa fa-arrow-right"></i></p>	
					                    </div>
					                    <div class="fileupload-preview fileupload-exists thumbnail col-4 my-auto bg-theme"></div>
				                      </div>
				                        <span class="btn btn-theme btn-file w-100 my-3">
				                         
				                        <input id="src" type="file" class="default @error('src') is-invalid @enderror" name="src" placeholder="fdfdffd" accept="image/png, image/jpeg" autofocus>

			                                @error('src')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
				                        </span>
				                        
				                    </div>
				      
				                  </div>
				                </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Guardar</button>
	                              <button class="btn btn-theme04" type="reset">Cancel</button>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>

    </section>
</section>




@endsection

@section('footer')

 <script  src="{{asset('lib/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
@endsection