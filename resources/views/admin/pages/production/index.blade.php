@extends('admin.layouts.app')

@section('title','Centros de Producciones ')

@section('header')


@endsection
@section('production')
active
@endsection
@section('content')
	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{{ route('admin.production.create') }}" class="btn btn-compose text-truncate">
                  <i class="fa fa-pencil"></i>  Agregar Centro
                  </a>
                <ul class="nav nav-pills nav-stacked mail-nav row">
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Activos
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Bloqueados
                  		<span class="label label-info pull-right inbox-notification">8</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Eliminados
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista de Centros de producción
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($productions as $production)
            						<tr class="">
            							<td class="inbox-small-cells">
            								<input type="checkbox" class="mail-checkbox">
            							</td>
            							<td class="view-message  dont-show"><a href="{{ route('admin.production.show', $production) }}">{{ $production->code}}</a></td>
            							<td class="view-message "><a href="{{ route('admin.production.show', $production) }}">{{ $production->name }}</a></td>
            							<td class="view-message ">
            								<a href="{{ route('admin.production.show', $production) }}">
            			                    	@if($production->status===0 )
            			                    		<i class="fa fa-lock"></i> eliminado
            			                    	@elseif($production->status==1)
            			                    		<i class="fa fa-unlock"></i> activo
            			                    	@else
            										<i class="fa fa-unlock"></i> Bloqueado
            			                    	@endif	
            								</a>
            							</td>
            							<td class="view-message  text-right">
            								<a href="{{ route('admin.production.edit', $production) }}" class="px-3"><i class="fa fa-pencil"></i></a>
            								<a href="{{ route('admin.production.update', $production->id) }}" class="px-3"><i class="fa fa-trash-o"></i></a>
            							</td>
            						</tr>
            						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>

	
@endsection

@section('footer')


@endsection