@extends('admin.layouts.app')

@section('title','Editar rol'.$role->name)

@section('header')


@endsection
@section('user')
active
@endsection
@section('role')
active
@endsection
@section('content')
	

<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav">

                   <div class="col-lg-10 col-md-9 col-8" >
                   	 <h4 class=" " >Editar Rol {{ $role->name }}<h4/>
                   </div>
 					<div class="my-auto col-lg-2 col-md-3 col-4">
 						<a class="btn btn-danger text-white float-right" href="{{ route('admin.role.show',$role)}}">Cancelar</a>
 					</div>                 
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form   class="form-horizontal" method="POST" action="{{ route('admin.role.update',$role) }}" >	
                        	@csrf
                        	{{ method_field('PUT') }}
                        	<div class="col-lg-6 col-md-12  ">


	                          <div class="form-group">
	                            <label class="col-12"> Nombre</label>
	                            <div class="col-12">
	                              	<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $role->name }}" required autocomplete="name" autofocus>

	                                @error('name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>

	                          
	                          <div class="form-group">
	                            <label class="col-12">Descripción</label>
	                            <div class="col-12">
	                              	<input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $role->description }}" required autocomplete="description" autofocus>

	                                @error('description')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          
	                        </div>
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Actualizar</button>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>

    </section>
</section>





@endsection

@section('footer')


@endsection