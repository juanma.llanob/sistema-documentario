@extends('admin.layouts.app')

@section('title',$role->name)

@section('header')


@endsection
@section('user')
active
@endsection
@section('role')
active
@endsection
@section('content')

<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <div class="nav ">

                  <div class="col-lg-10 col-md-9 col-8  my-3" >
                    <h4 class="text-truncate"> Role <strong>{{ $role->name}}</strong><h4/> 

                  </div>
                  <div class=" my-auto col-lg-2 col-md-3 col-4 ">
                  	<div class="dropdown  float-right">
          					  <button class="btn btn-theme dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          					    ACCIONES
          					  </button>
          					  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          					    <a class="dropdown-item" href="{{ route('admin.role.edit',$role)}}">Editar</a>
          	    				<a class="dropdown-item text-danger" href="#" onclick="enviar_formulario()" >Eliminar</a>

          					  </div>
          					</div>
                  </div>
                </div>
          
              </div>

              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        

                        	<div class="col-lg-6 col-md-12  ">

                        		<h3>Datos Rol</h3>
                        		<table class="table  ">
                        			
                    					<tr class="">
                  							<td class="" style="width:130px">
                  								Nombre del role:
                  							</td>
                  							<td class="">
                  								{{ $role->name}}
                  							</td>			
                    					</tr>
                    					<tr class="">
                  							<td class="" style="width:130px">
                  								Slug :
                  							</td>
                  							<td class="">
                  								{{ $role->slug}}
                  							</td>			
                    					</tr>
                    					<tr class="">
                  							<td class="" style="width:130px">
                  								Descripción :
                  							</td>
                  							<td class="">
                  								{!! nl2br(e($role->description)) !!}
                  							</td>			
                    					</tr>
                    					
                    					
                				
                				</table>
                			</div>
		                    <div class="col-lg-6 col-md-12  ">
	                        
	                       </div>
        				<!-- /col-lg-8 -->
                      </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
                <div class="col-sm-12">
                  <section class="panel">
                    <header class="panel-heading wht-bg">
                      <h4 class="gen-case">
                          Lista de permisos  :
                          <form action="#" class="pull-right mail-src-position">
                            <div class="input-append">
                              <input type="text" class="form-control " placeholder="Search Mail">
                            </div>
                          </form>
                        </h4>
                    </header>

                    <div class="panel-body minimal">
               
                      <div class="table-inbox-wrap ">
                        @if($permissions->count())
                        <table class="table table-inbox table-hover">
                          <thead>
                            <td> {{$permissions->count()}}</td>
                            <td>Nombre </td>
                            <td>Slug </td>
                            <td>Descripción </td>
                            <td>Opciones</td>
                            <td></td>

                          </thead>
                          <tbody>
                            

                              @foreach($permissions as $permission)
                                <tr class="">
                                  <td class="inbox-small-cells">
                                    <input type="checkbox" class="mail-checkbox">
                                  </td>
                                  <td class="view-message  dont-show">
                                    <a href="{{ route('admin.permission.show', $permission) }}">{{ $permission->name}}</a>
                                  </td>
                                  <td class="view-message ">
                                    <a href="{{ route('admin.permission.show',$permission) }}">{{ $permission->slug }}</a>
                                  </td>
                                  <td class="view-message ">
                                    <a href="{{ route('admin.permission.show',$permission) }}">{{ $permission->description }}</a>
                                  </td>
								  <td class="view-message ">
                                    <a href="{{ route('admin.permission.edit', $permission) }}" class="px-3"><i class="fa fa-pencil"></i></a>
                                  </td>
                                </tr>
                              @endforeach


                          </tbody>
                        </table>
                         @else
                              <p class="text-center">No tiene Permisos !
                              </p>
                          @endif  
                      </div>
                    </div>
                  </section>
                </div>
              </div>
              <!-- /panel-body -->

            </div>
            <!-- /col-lg-12 -->
    </div>
          

  </section>


</section>





	<form method="Post" action="{{ route('admin.role.destroy', $role) }}" name="delete_form">
		@csrf
	    {{ method_field('DELETE') }}
		
	</form>
@endsection

@section('footer')
<script type="text/javascript">
	function enviar_formulario()
	{
		swal({
			title: "¿quiere eliminar?",
			text: "Esta acción no se puede deshacer",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Sí, continuar",
			cancelButtonText: "No, Cancelar",
		
		}).then((result) => {
			if(result.value){
				document.delete_form.submit();
			}else{
				swal(
					'Operación cancelada',
					'Registro no eliminada',
					'error'
				)
			}
		});
	}
</script>

@endsection