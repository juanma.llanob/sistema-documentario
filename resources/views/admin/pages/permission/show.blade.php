@extends('admin.layouts.app')

@section('title',$permission->name)

@section('header')


@endsection
@section('user')
active
@endsection
@section('permission')
active
@endsection
@section('content')
	<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <div class="nav nav-tabs ">

                  <div class="col-lg-10 col-md-9 col-8  my-3" >
                    <h4 class="text-truncate"> Permiso <strong>{{ $permission->name}}</strong><h4/> 

                  </div>
                  <div class=" my-auto col-lg-2 col-md-3 col-4 ">
                  	<div class="dropdown  float-right">
          					  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          					    ACCIONES
          					  </button>
          					  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          					    <a class="dropdown-item" href="{{ route('admin.permission.edit',$permission)}}">Editar</a>
          	    				<a class="dropdown-item text-danger" href="#" onclick="enviar_formulario()" >Eliminar</a>

          					  </div>
          					</div>
                  </div>
                </div>
          
              </div>

              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        

                        	<div class="col-lg-6 col-md-12  ">

                        		<h3>Datos partidoles</h3>
                        		<table class="table  ">
                        			
                    					<tr class="">
                  							<td class="" style="width:130px">
                  								Nombre del permission:
                  							</td>
                  							<td class="">
                  								{{ $permission->name}}
                  							</td>			
                    					</tr>
                    					<tr class="">
                  							<td class="" style="width:130px">
                  								Slug :
                  							</td>
                  							<td class="">
                  								{{ $permission->slug}}
                  							</td>			
                    					</tr>
                    					<tr class="">
                  							<td class="" style="width:130px">
                  								Descripción :
                  							</td>
                  							<td class="">
                  								{!! nl2br(e($permission->description)) !!}
                  							</td>			
                    					</tr>
                    					
                    					
                				
                				</table>
                			</div>
		                    <div class="col-lg-6 col-md-12  ">
	                        
	                       </div>
        				<!-- /col-lg-8 -->
                      </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->


            </div>
            <!-- /col-lg-12 -->
    </div>
          

  </section>


</section>





	<form method="Post" action="{{ route('admin.permission.destroy', $permission) }}" name="delete_form">
		@csrf
	    {{ method_field('DELETE') }}
		
	</form>
@endsection

@section('footer')
<script type="text/javascript">
	function enviar_formulario()
	{
		swal({
			title: "¿quiere eliminar?",
			text: "Esta acción no se puede deshacer",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Sí, continuar",
			cancelButtonText: "No, Cancelar",
		
		}).then((result) => {
			if(result.value){
				document.delete_form.submit();
			}else{
				swal(
					'Operación cancelada',
					'Registro no eliminada',
					'error'
				)
			}
		});
	}
</script>

@endsection