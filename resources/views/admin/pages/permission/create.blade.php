@extends('admin.layouts.app')

@section('title','crear')

@section('header')


@endsection
@section('user')
active
@endsection
@section('permission')
active
@endsection
@section('content')


	<div class="col-12">
		<div class="card card-small mb-4">
			<div class="card-header border-bottom">
				<h6 class="m-0">Agregar nuevo Permiso</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12 col-lg-6 m-auto">
						<form method="POST" action="{{ route('admin.permission.store') }}">
							 @csrf
							<div class=" ">
								<label for="name" class="col-form-label text-muted">{{ __('Nombre') }}</label>
								<div class="input-group ">
									<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus placeholder="Master Administrador"> 
									@error('name')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
								</div>
							</div>
							<div class=" ">
								<label for="role_id" class="col-form-label text-muted">{{ __('Rol') }}</label>
								<div class="input-group ">
									<select name="role_id" class="form-control @error('role_id') is-invalid @enderror">
										<option value="" disabled="" selected="">Selecciona un rol</option>
										@foreach($roles as $role)
										<option value="{{ $role->id }}">{{ $role->name }}</option>
										@endforeach
									</select>

									@error('role_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
								</div>
							</div>

							<div class="mb-3">
								<label for="description" class="col-form-label text-muted">{{ __('Descripción') }}</label>
								<div class="input-group ">
									<input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}"  autocomplete="description" autofocus placeholder="Controla todo">
									@error('description')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
					            </div>    
							</div>
							<div class="form-group  mb-0">				          
					            <button type="submit" class="btn btn-primary">
					                {{ __('Registrar') }}
					            </button>
					        </div>
						</form>
					</div>
				</div>
			</div>
		</div>    
	</div>
	
	

@endsection

@section('footer')


@endsection