@extends('admin.layouts.app')

@section('title','Editar'.$user->name)

@section('header')


@endsection
@section('user')
active
@endsection
@section('admin')
active
@endsection
@section('content')
		

	<section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
              <div class="col-md-4 profile-text mt mb centered">
                <div class="right-divider hidden-sm hidden-xs">
                  <h4>{{$user->phone}}</h4>
                  <h6>Teléfono</h6>
                  <h4>{{$user->dni}}</h4>
                  <h6>DNI</h6>

                  @switch($user->status)
                  @case(0)
                  <h4>Borrado</h4>
                  @break
                  @case(1)
                  <h4>Activo</h4>
                  @break
                  @case(2)
                  <h4>Bloqueado</h4>
                  @break
                  @default
                  <h6>...</h6>
                  @endswitch
                  <h6>Estado</h6>
                </div>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 profile-text">
                <h3>{{ $user->surname.' '.$user->name}}</h3>
                <h6> Administrador</h6>
                <p>Este usuario fue registrado el {{ $user->created_at }}, pertenece al centro de producción <a class="link-theme" href="{{ route('admin.production.show', $user->production )}}">{{ $user->production->name}}</a></p>
                <br>
                <p><button class="btn btn-theme"><i class="fa fa-envelope"></i> Datos</button></p>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 centered">
                <div class="profile-pic">
                  <p><img src="{{ Storage::url($user->src) }}" class="img-circle"></p>
                  <p>
                  	<a class="btn btn-theme04" href="{{ route('admin.user.show',$user) }}"> <i class="fa fa-close"></i> Cancelar Edición</a>
                  </p>
                </div>
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /col-lg-12 -->
          <div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">
                  <li class="">
                    <a data-toggle="tab" href="#roles">Roles</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#permisos" class="">Permisos</a>
                  </li>
                  <li class="">
                    <a data-toggle="tab" href="#perfil">Perfil</a>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">
                <div class="tab-content ">
                  <div id="roles" class="tab-pane ">
                    <div class="row">
                      <!-- /col-md-6 -->
                      <div class="col-lg-offset-3 col-md-6 detailed">
                        	
                      	<form method="POST" action="{{ route('admin.user.role_assignment',$user) }}">
	                        @csrf

	                        <div class="steps">

	                            @foreach($roles as $role)
	                            		<label>
		                                <input   type="checkbox" class=" " name="roles[]" @if($user->has_role($role->id)) checked @endif value="{{ $role->id }}"  />
		                                {{ $role->name }} </label>
		                                
	                            @endforeach
	                            <input type='submit' value='Actualizar' id='submit' />
	                        </div>
	                        
	                        
	                    </form>

			              
                      </div>
                      <!-- /col-md-6 -->
                    </div>
                    <!-- /OVERVIEW -->
                  </div>
                  <!-- /tab-pane -->
                  <div id="permisos" class="tab-pane">
                  	<form method="POST" action="{{ route('admin.user.permission_assignment',$user) }}">
	                    @csrf
                    	<div class=" justify-content-center">
                            @foreach($role_permissions as $role)
                            	<div class=" col-md-4 ">
                            		<p class="m-0 ml-3 ">{{ $role->name }}</p>
                            		<div class="steps">
                            		@foreach($role->permissions as $permission)

                            			<label>
		                                <input   type="checkbox" class=" " name="permissions[]"  value="{{ $permission->id }}" @if($user->has_permission($permission->id)) checked @endif />
		                                {{ $permission->name }} 
		                            	</label>
	                               @endforeach
	                           		</div>
                                </div>
                            @endforeach
	                        <div class="steps m-3">
                    			<input type='submit' value='Actualizar Administrador' id='submit' />
                    		</div>                    
	                    </div>
	                    
	                </form>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                  <div id="perfil" class="tab-pane ">
                  	 <h4 class="mb">Personal Information</h4>
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.user.update',$user) }}" enctype="multipart/form-data">
                        	@csrf
	                        {{ method_field('PUT') }}

                        	<div class="col-lg-6 col-md-12  ">
	                          <div class="form-group">
	                            <label class="col-12"> Nombre</label>
	                            <div class="col-12">
	                              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
	                              @error('name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Apellido</label>
	                            <div class="col-12">
	                              <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $user->surname }}" required autocomplete="surname" autofocus>

	                                @error('surname')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12"> DNI</label>
	                            <div class="col-12">
	                              <input id="dni" type="number" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{ $user->dni }}" required autocomplete="dni" autofocus>

	                                @error('dni')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Teléfono</label>
	                            <div class="col-12">
	                              <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ $user->phone }}" required autocomplete="phone" autofocus>

	                                @error('phone')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Tipo de Trabajo</label>
	                            <div class="col-12">
	                              <select name="jobtype_id" class="form-control @error('jobtype_id') is-invalid @enderror" required>
										<option value="{{ $user->jobtype->id }}" selected="">  {{ $user->jobtype->name }}</option>
										@foreach($jobtypes as $jobtype)
											@if($user->jobtype->id !== $jobtype->id)
											<option value="{{ $jobtype->id }}">{{ $jobtype->name }}</option>
											@endif
										@endforeach
									</select>

									@error('jobtype_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Centro de Producción</label>
	                            <div class="col-12">
	                              <select name="production_id" class="form-control @error('production_id') is-invalid @enderror" required>
										<option value="{{ $user->production->id }}" selected=""> {{ $user->production->name }} </option>
										@foreach($productions as $production)
											@if($user->production->id !== $production->id )
											<option value="{{ $production->id }}">{{ $production->name }}</option>
											@endif
										@endforeach
									</select>

									@error('production_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Correo</label>
	                            <div class="col-12">
	                              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

	                                @error('email')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Estado</label>
	                            <div class="col-12">
	                              
	                              	<select name="status" class="form-control @error('status') is-invalid @enderror" required>
										<option value="{{ $user->status }}" selected=""> 
											@if($user->status===0)
												Eliminado </option>
												<option value="1"> Activo</option>
										 		<option value="2"> Bloqueado</option>
											@elseif($user->status ===1)
												Activo </option>
												<option value="0"> Eliminado</option>
												<option value="2"> Bloqueado</option>
											@else
												Bloqueado</option>
												<option value="0"> Eliminado</option>
										 		<option value="1"> Activo</option>
											@endif
										
									</select>

	                                @error('status')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Contraseña</label>
	                            <div class="col-12">
	                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" value="{{ $user->password }}">

	                                @error('password')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Confirmar Contraseña</label>
	                            <div class="col-12">
	                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" value="{{ $user->password }}">
	                            </div>
	                          </div>

	                          
	                        </div>
	                        <div class="col-lg-6 col-md-12  ">
	                        	

	                        	<div class="form-group ">

				                  <div class="col-12 text-center">
				                    <div class="fileupload fileupload-new " data-provides="fileupload">
				                      <div class="col-12 row">
				                      	<div class="fileupload-new thumbnail col-4 my-auto " >
					                        <img src="{{ Storage::url($user->src) }}"   />
					                    </div>
					                    <div class="col-4 my-auto">
											<p class="link-theme text-center"> Cambiar Imagen <i class="fa fa-arrow-right"></i></p>	
					                    </div>
					                    <div class="fileupload-preview fileupload-exists thumbnail col-4 my-auto bg-theme"></div>
				                      </div>
				                        <span class="btn btn-theme btn-file w-100 my-3">
				                         
				                        <input id="src" type="file" class="default @error('src') is-invalid @enderror" name="src" placeholder="fdfdffd" accept="image/png, image/jpeg" autofocus>

			                                @error('src')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
				                        </span>
				                        
				                    </div>
				      
				                  </div>
				                </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Actualizar</button>
	                              <button class="btn btn-theme04" type="reset">Cancel</button>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          <!-- /row -->
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>


@endsection

@section('footer')
  <script  src="{{asset('lib/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>


@endsection