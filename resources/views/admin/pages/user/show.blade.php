@extends('admin.layouts.app')

@section('title', $user->name )

@section('header')


@endsection
@section('user')
active
@endsection
@section('admin')
active
@endsection
@section('content')
<section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row content-panel">
              <div class="col-md-4 profile-text mt mb centered">
                <div class="right-divider hidden-sm hidden-xs">
                  <h4>{{$user->phone}}</h4>
                  <h6>Teléfono</h6>
                  <h4>{{$user->dni}}</h4>
                  <h6>DNI</h6>

                  @switch($user->status)
                  @case(0)
                  <h4>Bloqueado</h4>
                  @break
                  @case(1)
                  <h4>Activo</h4>
                  @break
                  @case(2)
                  <h4>Eliminado</h4>
                  @break
                  @default
                  <h6>...</h6>
                  @endswitch
                  <h6>Estado</h6>
                </div>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 profile-text">
                <h3>{{ $user->surname.' '.$user->name}}</h3>
                <h6> Administrador</h6>
                <p>Este usuario fue registrado el {{ $user->created_at }}, pertenece al centro de producción <a class="link-theme" href="{{ route('admin.production.show', $user->production )}}">{{ $user->production->name}}</a></p>
                <br>
                <p><button class="btn btn-theme"><i class="fa fa-envelope"></i> Datos</button></p>
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 centered">
                <div class="profile-pic">
                  <p><img src="{{ Storage::url($user->src) }}" class="img-circle"></p>
                  <p class="row justify-content-center">
                  	@if(Auth::user()->id!==$user->id )
                    	@if($user->status===1 )
                    		<form method="Post" action="{{ route('admin.user.destroy', $user) }}" class="" >
                          @csrf
                            {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">Bloquear</button>
                        </form>
                    	@else
                    		<form method="Post" action="{{ route('admin.user.update_all', $user) }}">
                          @csrf
                            <button class="btn btn-theme" href="#" ><i class="fa fa-unlock"></i> Desbloquear </button>
                        </form>
                    	@endif	
                    @endif
                    <a  href="{{ route('admin.user.edit',$user) }}" class="btn btn-theme02"><i class="fa fa-pencil"></i> Editar</a>
                  </p>
                </div>
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /col-lg-12 -->
          <div class="col-lg-12 mt">
            <div class="row content-panel">
             
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          <!-- /row -->
        </div>
        <!-- /container -->
      </section>
      <!-- /wrapper -->
    </section>

	<form method="Post" action="{{ route('admin.user.destroy', $user) }}" name="delete_form">
		@csrf
	    {{ method_field('DELETE') }}
		
	</form>
@endsection

@section('footer')
<script type="text/javascript">
	function actualizar_estado()
	{
		swal({
			title: "¿quiere eliminar?",
			text: "Esta acción no se puede deshacer",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Sí, continuar",
			cancelButtonText: "No, Cancelar",
		
		}).then((result) => {
			if(result.value){
				document.delete_form.submit();
			}else{
				swal(
					'Operación cancelada',
					'Registro no eliminada',
					'error'
				)
			}
		});
	}
</script>

@endsection