@extends('admin.layouts.app')

@section('title','Asignar Permiso')

@section('header')


@endsection
@section('user')
active
@endsection
@section('admin')
active
@endsection
@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-9">
	            <div class="card">
	                <div class="card-header">{{ __('Asignar Permiso') }} : {{ $user->name}}</div>

	                <div class="card-body">
	                    <form method="POST" action="{{ route('admin.user.permission_assignment',$user) }}">
	                        @csrf

	                        <div class="row">

	                            @foreach($roles as $role)
	                            	
	                            		<p class="m-0 ml-3 ">{{ $role->name }}</p>
	                            		@foreach($role->permissions as $permission)
	                            		 <div class=" col-12">
			                                <input   type="checkbox" class=" " name="permissions[]"  value="{{ $permission->id }}" @if($user->has_permission($permission->id)) checked @endif />
			                                <label for="name" class=""> {{ $permission->name }} </label>
			                            </div>
		                               @endforeach
	                                
	                            @endforeach
	                            
	                        </div>
	                        
	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-primary">
	                                    {{ __('Guardar') }}
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	        <div class="col-3">
	       	 @include('admin.layouts.includes.box')
	       </div>
	    </div>
	</div>
@endsection

@section('footer')


@endsection