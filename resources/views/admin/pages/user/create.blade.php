@extends('admin.layouts.app')

@section('title','Crear Usuario')

@section('header')


@endsection

@section('user')
active
@endsection
@section('admin')
active
@endsection
@section('content')

<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">

                  <li class="">
                    <h4 >Registro de  Usuario<h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.user.store') }}" enctype="multipart/form-data">
                        	@csrf

                        	<div class="col-lg-6 col-md-12  ">

                        	  <div class="form-group">
	                            <label class="col-12"> Rol</label>
	                            <div class="col-12">
	                              	<select name="role" class="form-control @error('role') is-invalid @enderror" required>
										<option value="" disabled="" selected=""> Escoja un Rol </option>
										@foreach($roles as $role)
										<option value="{{ $role->id }}">{{ $role->name }}</option>
										@endforeach
									</select>

									@error('role')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12"> Nombre</label>
	                            <div class="col-12">
	                              	<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

	                                @error('name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Apellido</label>
	                            <div class="col-12">
	                              <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>

	                                @error('surname')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12"> DNI</label>
	                            <div class="col-12">
	                              <input id="dni" type="number" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{ old('dni') }}" required autocomplete="dni" autofocus>

	                                @error('dni')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Teléfono</label>
	                            <div class="col-12">
	                              <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

	                                @error('phone')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Tipo de Trabajo</label>
	                            <div class="col-12">
	                             	<select name="jobtype_id" class="form-control @error('jobtype_id') is-invalid @enderror" required>
										<option value="" disabled="" selected=""> Escoja tipo de trabajo </option>
										@foreach($jobtypes as $jobtype)
										<option value="{{ $jobtype->id }}">{{ $jobtype->name }}</option>
										@endforeach
									</select>

									@error('jobtype_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Centro de Producción</label>
	                            <div class="col-12">
	                              	<select name="production_id" class="form-control @error('production_id') is-invalid @enderror" required>
										<option value="" disabled="" selected=""> Escoja un Centro </option>
										@foreach($productions as $production)
											@if($production->is_active($production))
											<option value="{{ $production->id }}">{{ $production->name }}</option>
											@endif
										@endforeach
									</select>

									@error('production_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Correo</label>
	                            <div class="col-12">
	                              	<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

	                                @error('email')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Estado</label>
	                            <div class="col-12">
	                              
	                              	<select name="status" class="form-control @error('status') is-invalid @enderror" required>
										
										<option value="1"> Activo</option>
								 		<option value="2"> Bloqueado</option>
										<option value="0"> Eliminado</option>

									</select>

	                                @error('status')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Contraseña</label>
	                            <div class="col-12">
	                              	<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

	                                @error('password')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Confirmar Contraseña</label>
	                            <div class="col-12">
	                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
	                            </div>
	                          </div>

	                          
	                        </div>
	                        <div class="col-lg-6 col-md-12  ">
	                        	

	                        	<div class="form-group ">

				                  <div class="col-12 text-center">
				                    <div class="fileupload fileupload-new " data-provides="fileupload">
				                      <div class="col-12 row justify-content-center">
				                      	<span class="btn btn-theme btn-file w-100 my-3">
				                         
				                        <input id="src" type="file" class="default @error('src') is-invalid @enderror" name="src" placeholder="" accept="image/png, image/jpeg" autofocus>

			                                @error('src')
			                                    <span class="invalid-feedback" role="alert">
			                                        <strong>{{ $message }}</strong>
			                                    </span>
			                                @enderror
				                        </span>
					                    
					                    <div class="fileupload-preview fileupload-exists thumbnail col-4 my-auto bg-theme02"></div>
				                      </div>
				                        
				                        
				                    </div>
				      
				                  </div>
				                </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Guardar</button>
	                              <button class="btn btn-theme04" type="reset">Cancel</button>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>

    </section>
</section>

	
@endsection

@section('footer')
  <script  src="{{asset('lib/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>


@endsection