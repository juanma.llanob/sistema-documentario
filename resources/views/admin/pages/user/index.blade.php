@extends('admin.layouts.app')

@section('title','Usuarios del Sistemas')

@section('header')


@endsection
@section('user')
active
@endsection
@section('admin')
active
@endsection
@section('content')
	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{{ route('admin.user.create')}}" class="btn btn-compose text-truncate">
                  <i class="fa fa-pencil"></i>  Agregar Usuario
                  </a>
                <ul class="nav nav-pills nav-stacked mail-nav row">
                  <li class="active w-100">
                  	<a href=""> 
                  		<i class="fa fa-user"></i> Administradores  
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Encargados
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Usuarios
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Bloqueados
                  		<span class="label label-info pull-right inbox-notification">8</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Eliminados
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista de Registrados
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($users as $user)
            						<tr class="">
            							<td class="inbox-small-cells">
            								<input type="checkbox" class="mail-checkbox">
            							</td>
            							<td class="view-message  dont-show"><a href="{{ route('admin.user.show', $user) }}">{{ $user->surname.' '.$user->name}}</a></td>
            							<td class="view-message "><a href="{{ route('admin.user.show', $user) }}">{{ $user->production->name }}</a></td>
            							<td class="view-message ">
            								<a href="{{ route('admin.user.show', $user) }}">
            			                    	@if($user->status===0 )
            			                    		<i class="fa fa-lock"></i> eliminado
            			                    	@elseif($user->status==1)
            			                    		<i class="fa fa-unlock"></i> activo
            			                    	@else
            										<i class="fa fa-unlock"></i> Bloqueado
            			                    	@endif	
            								</a>
            							</td>
            							<td class="view-message  text-right">
            								<a href="{{ route('admin.user.edit', $user) }}" class="px-3"><i class="fa fa-pencil"></i></a>
            								<a href="{{ route('admin.user.update', $user->id) }}" class="px-3"><i class="fa fa-trash-o"></i></a>
            							</td>
            						</tr>
            						@endforeach

                    </tbody>
                  </table>
                  <div class="w-100 text-right">
                    {!! $users->links() !!}
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>

	
@endsection

@section('footer')


@endsection