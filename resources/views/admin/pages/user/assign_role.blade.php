@extends('admin.layouts.app')

@section('title','Asignar Rol')

@section('header')


@endsection
@section('user')
active
@endsection
@section('admin')
active
@endsection
@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-9">
	            <div class="card">
	                <div class="card-header">{{ __('Asignar Rol') }}</div>

	                <div class="card-body">
	                    <form method="POST" action="{{ route('admin.user.role_assignment',$user) }}">
	                        @csrf

	                        <div class="form-group row">

	                            @foreach($roles as $role)
	                            	<div class="form-group col-12">
	                            		
		                                <input   type="checkbox" class=" " name="roles[]" @if($user->has_role($role->id)) checked @endif value="{{ $role->id }}"  />
		                                <label for="name" class=""> {{ $role->name }} </label>
		                                
	                                </div>
	                            @endforeach
	                            
	                        </div>
	                        
	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-primary">
	                                    {{ __('Guardar') }}
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	       <div class="col-3">
	       	 @include('admin.layouts.includes.box')
	       </div>
	    </div>
	</div>
@endsection

@section('footer')


@endsection