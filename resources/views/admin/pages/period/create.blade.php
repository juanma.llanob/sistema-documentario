@extends('admin.layouts.app')

@section('title','crear')

@section('header')


@endsection
@section('period')
active
@endsection
@section('content')


<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">

                  <li class="">
                    <h4 >Registro nuevo  Periodo<h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.period.store') }}" >
                        	@csrf

                        	<div class="col-lg-6 col-md-12  ">


	                          <div class="form-group">
	                            <label class="col-12"> Año</label>
	                            <div class="col-12">
	                              	<input id="year" type="number" class="form-control @error('year') is-invalid @enderror" name="year" value="{{ old('year') }}" required autocomplete="year" autofocus>

	                                @error('year')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12"> Mes</label>
	                            <div class="col-12">
	                              	<input id="month" type="number" class="form-control @error('month') is-invalid @enderror" name="month" value="{{ old('month') }}" required autocomplete="month" autofocus>

	                                @error('month')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12">Límite</label>
	                            <div class="col-12">
	                              	<input id="limiter" type="date" class="form-control @error('limiter') is-invalid @enderror" name="limiter" value="{{ old('limiter') }}" required autocomplete="limiter" autofocus>

	                                @error('limiter')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Estado</label>
	                            <div class="col-12">
	                              
	                              	<select name="status" class="form-control @error('status') is-invalid @enderror" required>
										
										<option value="1"> Activo</option>
								 		<option value="2"> Bloqueado</option>
										<option value="0"> Eliminado</option>

									</select>

	                                @error('status')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                        </div>  
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Guardar</button>
	                              <button class="btn btn-theme04" type="reset">Cancel</button>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>

    </section>
</section>






	
	

@endsection

@section('footer')


@endsection