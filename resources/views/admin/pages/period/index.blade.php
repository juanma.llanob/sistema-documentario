@extends('admin.layouts.app')

@section('title','Periodos')

@section('header')


@endsection
@section('period')
active
@endsection
@section('content')

	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
              		<a href="{{ route('admin.period.create')}}" disable="" class="btn btn-compose text-truncate">
                  		<i class="fa fa-pencil"></i>  Agregar Periodo
                	</a>
                <ul class="nav nav-pills nav-stacked mail-nav row">
                   <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> 2018
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> 2019
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> 2020
                  		<span class="label label-info pull-right inbox-notification">8</span>
                  	</a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista de Periodos
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($periods as $period)
                    	  <tr @if($period->status) class="unread" @endif >
	
              							<td class="inbox-small-cells">
              								<input type="checkbox" class="mail-checkbox">
              							</td>
              							<td class="view-message "><a href="{{ route('admin.period.show', $period) }}">{{ $period->year }}</a></td>
              							<td class="view-message "><a href="{{ route('admin.period.show', $period) }}">{{ $period->month }}</a></td>
              							<td class="view-message ">
              								<a href="{{ route('admin.period.show', $period) }}">
  			                    	@if($period->status===0 )
  			                    		<i class="fa fa-lock"></i> inactivo
  			                    	@elseif($period->status===1)
  			                    		<i class="fa fa-unlock"></i> activo 
  			                    	@endif	
              								</a>
            							</td>
            							
            							<td class="view-message ">
            								<a href="{{ route('admin.period.show', $period) }}" class="px-3">
            									{{ $period->limiter }}
            								</a>
            							</td>
                          <td class="view-message "><a href="{{ route('admin.period.edit', $period) }}" class="px-3"><i class="fa fa-pencil"></i></a></td>
            						</tr>
            					@endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>



	
@endsection

@section('footer')


@endsection