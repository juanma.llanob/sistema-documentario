@extends('admin.layouts.app')

@section('title','Perido')

@section('header')


@endsection
@section('period')
active
@endsection
@section('content')
	
	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">

              	<div lass="btn btn-theme text-truncate ">
              		 @if($period->exists($period))
                    Estás en el periodo actual
                   @endif
              	</div>

                <ul class="nav nav-pills nav-stacked mail-nav row">
                   <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> 2018
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> 2019
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> 2020
                  		<span class="label label-info pull-right inbox-notification">8</span>
                  	</a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista del periodo{{ $period->year.'-'.$period->month }}
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($finances as $finance)
	                     <tr>
          							<td class="inbox-small-cells">
          								<input type="checkbox" class="mail-checkbox">
          							</td>
          							<td class="view-message "><a href="{{ route('admin.finance.show', $finance) }}">{{ $finance->code }}</a></td>
          							<td class="view-message "><a href="{{ route('admin.finance.show', $finance) }}">{{ $finance->production->name }}</a></td>
          							<td class="view-message ">
          								<a href="{{ route('admin.finance.show', $finance) }}">S/ {{ $finance->quantity }}</a>
          							</td>
          							<td class="view-message ">
          								<a href="{{ route('admin.finance.show', $finance) }}">{{ $finance->observation }}</a>
          							</td>
          							<td class="view-message "><a href="{{ route('admin.finance.show', $finance) }}" class="px-3"><i class="fa fa-pencil"></i></a></td>
          							<td class="view-message  text-right">
          								<a href="{{ route('admin.finance.show', $finance) }}" class="px-3">
          									{{ $finance->date }}
          								</a>
          							</td>
          							<td class="view-message  text-right">
          								<a href="{{ route('admin.finance.show', $finance) }}" class="px-3">
          									{{ $finance->updated_at }}
          								</a>
          							</td>
          						</tr>
          						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>

	
@endsection

@section('footer')


@endsection