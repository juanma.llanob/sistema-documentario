@extends('admin.layouts.app')

@section('title','Documentos Fianciero : '.$production->name)

@section('header')


@endsection
@section('document')
active
@endsection
@section('finance')
active
@endsection
@section('content')

	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
                <a href="{{ route('admin.finance.create') }}" class="btn btn-compose text-truncate">
                  <i class="fa fa-pencil"></i>  Agregar 
                  </a>
                <ul class="nav nav-pills nav-stacked mail-nav row">
                  <li class="active w-100">
                  	<a href=""> 
                  		<i class="fa fa-user"></i> Datos  
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista de Financiero de {{ $production->name.' de '.$finance->year.'-'.$finance->month}}
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($finances as $finance)
          						<tr class="">
          							<td class="inbox-small-cells">
          								<input type="checkbox" class="mail-checkbox">
          							</td>
          							<td class="view-message  dont-show"><a href="{{ route('admin.finance.show', $finance) }}">{{ $finance->code}}</a></td>
          							
          							<td class="view-message ">{{ $finance->quantity }}</td>
          							<td class="view-message ">{{ $finance->observation }}</td>
          							<td class="view-message ">{{ $finance->date }}</td>
          							<td class="view-message  text-right">
          								<a href="{{ route('admin.finance.edit', $finance) }}" class="px-3"><i class="fa fa-pencil"></i></a>
          							</td>
                        <td>
                          <form method="POST" action="{{ route('admin.finance.destroy',$finance)}}" >
                          @csrf
                          {{ method_field('DELETE') }}
                          <button type="submit" class="btn-link ">
                            <i class="fa fa-trash-o text-danger"></i>
                          </button>
                        </form>
                        </td>
          						</tr>
          						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>


        <div class="col-lg-12 mt">
          <div class="row content-panel">
            <div class="col-lg-10 col-lg-offset-1">
              <div class="invoice-body">
                <div class="pull-left">
                  <h1>OUCP</h1>
                  <address>
                <strong>{{ $production->name}}</strong><br>
                <i class="fa fa-envelope"></i>: {{ $production->email }}<br>
                Código: {{ $production->code }}
                <br>
                <abbr title="Phone"><i class="fa fa-phone"></i></abbr>: {{ $production->phone}}
              </address>
                </div>
                <!-- /pull-left -->
                <div class="pull-right">
                  <h2>UNSA</h2>
                </div>
                <!-- /pull-right -->
                <div class="clearfix"></div>
                <br>
                <br>
                <br>
                <div class="row">
                  <div class="col-md-9">
                    <h4>{{ $production->name}}</h4>
                    <address>
		                <strong>{{ $production->name}}</strong><br>
		                <i class="fa fa-envelope"></i>: {{ $production->email }}<br>
		                Código: {{ $production->code }}
		                <br>
		                <abbr title="Phone"><i class="fa fa-phone"></i></abbr>: {{ $production->phone}}
		              </address>
                  </div>
                  <!-- /col-md-9 -->
                  <div class="col-md-3">
                    <br>
                    <div>
                      <div class="pull-left"> DOCUMENTO COD : </div>
                      <div class="pull-right"> 000283 </div>
                      <div class="clearfix"></div>
                    </div>
                    <div>
                      <!-- /col-md-3 -->
                      <div class="pull-left"> FECHA : </div>
                      <div class="pull-right"> {{ date('d/m/Y') }} </div>
                      <div class="clearfix"></div>
                    </div>
                    <!-- /row -->
                    <br>
                    <div class="well well-small green">
                      <div class="pull-left"> Total  : </div>
                      <div class="pull-right"> {{$total}} </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <!-- /invoice-body -->
                </div>
                <!-- /col-lg-10 -->
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width:60px" class="text-center">Nº</th>
                      <th class="text-left">CODIGO</th>
                      <th class="text-left">DESCRIPCION</th>
                      <th class="text-left">FECHA</th>
                      <th style="width:140px" class="text-right">PRECIO UNIT</th>
                      <th style="width:90px" class="text-right">TOTAL</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($finances as $finance)
                    	<tr>
	                      <td class="text-center">1</td>
	                      <td class="text-left">{{ $finance->code}}</td>
	                      <td class="text-left">{{ $finance->observation }}</td>
	                      <td class="text-left">{{ $finance->date }}</td>
	                      <td class="text-right">${{ $finance->quantity }}</td>
	                      <td class="text-right">${{ $finance->quantity }}</td>
	                    </tr>
                    @endforeach
                    
                    <tr>
                      <td colspan="4" rowspan="4">
                        <h4>cosas adicionales </h4>
                        <p>se puede cambiar mas.</p>
                        <td class="text-right"><strong>Subtotal</strong></td>
                        <td class="text-right">{{$total}}</td>
                    </tr>
                    
                    <tr>
                      <td class="text-right no-border">
                        <div class="well well-small green"><strong>Total</strong></div>
                      </td>
                      <td class="text-right"><strong>{{$total}}</strong></td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <br>
              </div>
      </section>
      

    </section>


      
	
@endsection

@section('footer')


@endsection