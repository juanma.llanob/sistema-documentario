@extends('admin.layouts.app')

@section('title','Financiero')

@section('header')


@endsection
@section('document')
active
@endsection
@section('finance')
active
@endsection
@section('content')
	
		<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">

                    <a href="{{ route('admin.finance.create') }}" disable="" class="btn btn-compose text-truncate">
                      <i class="fa fa-pencil"></i>  Agregar
                    </a>

                <ul class="nav nav-pills nav-stacked mail-nav row">
                   <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Activos
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Vencidos
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                 
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    
                    @if($period)
                      Lista de finanzas  {{ $period->year.'-'.$period->month}}
                    @else
                      ------------
                    @endif  
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
                  @if($period)
                    <div class="table-inbox-wrap ">
                      <table class="table table-inbox table-hover">
                        <thead>
                          <td>
                            
                          </td>
                          <td>
                            C. Producción
                          </td>
                          <td>
                            Estado 
                          </td>
                          <td>
                            Monto total
                          </td>
                        </thead>
                        <tbody>
                        	@foreach($productions as $production)
                						
                						<tr>
                							<td class="inbox-small-cells">
                								<input type="checkbox" class="mail-checkbox">
                							</td>
                							<td class="view-message "><a href="{{ route('admin.finance.show_detail',$production) }}">{{ $production->name }}</a></td>
                              <td class="view-message">
                                @if($production->has_finance_inPeriod($production->id))
                                  Enviado
                                @endif
                              </td>
                              
                							<td class="view-message ">S/.{{ $production->production_month_total($production->id) }}</a></td>            		
                						</tr>
                					@endforeach
                        </tbody>
                      </table>
                      <div class="w-100 text-center">
                    @else
                      No Hay periodo activo, <a href="{{ route('admin.period.create') }}"> Crear periodo</a>
                    @endif  
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>



@endsection

@section('footer')


@endsection