@extends('admin.layouts.app')

@section('title','Crear Financiero')

@section('header')
  <link href="{{asset('lib/dropzone/css/dropzone.css')}}" rel="stylesheet" />


@endsection
@section('document')
active
@endsection
@section('finance')
active
@endsection
@section('content')

		
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">

                  <li class="">
                    <h4 >Crear Documento Financiero <h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.finance.store') }}" >
                        	@csrf
                        	<div class="col-lg-6 col-md-12  ">


	                          <div class="form-group">
	                            <label class="col-12"> Codigo</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ old('code') }}"  autocomplete="code" autofocus placeholder=""> 
									@error('code')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12"> Fecha del recibo</label>
	                            <div class="col-12">
	                              	<input type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ old('date') }}"  autocomplete="date" autofocus placeholder="00-00-0000"> 
									@error('date')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12">Monto</label>
	                            <div class="col-12">
	                              	<input type="number" step="0.01" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ old('quantity') }}"  autocomplete="quantity" autofocus placeholder="000.00"> 
									@error('quantity')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12"> Observación</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('observation') is-invalid @enderror" name="observation" value="{{ old('observation') }}"  autocomplete="observation" autofocus placeholder=""> 
									@error('observation')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Periodo</label>
	                            <div class="col-12">
	                              	<select name="period_id" class="form-control @error('period_id') is-invalid @enderror" required>
										<option value="" disabled="" selected=""> Escoja el periodo </option>
										@foreach($periods as $period)
											
											<option value="{{ $period->id }}">{{ $period->year.'-'.$period->month }}</option>

										@endforeach
									</select>

									@error('period_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          
	                          <div class="form-group">
	                            <label class="col-12">Centro de Producción</label>
	                            <div class="col-12">
	                              
	                              	<select name="production_id" class="form-control @error('production_id') is-invalid @enderror" required>
										<option value="" disabled="" selected=""> Escoja un Centro </option>
										@foreach($productions as $production)
											@if($production->is_active($production) && $production->id!==1)
												<option value="{{ $production->id }}">{{ $production->name }}</option>
											@endif
										@endforeach
									</select>

									@error('production_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror

	                                @error('status')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                          </div>
	                          
	                          
	                        </div>
	                        
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Guardar</button>
	                              <button class="btn btn-theme04" type="reset">Cancel</button>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          <center ><h1 class="pt-4" > o </h1></center>
          
	        <div class="  w-100">
	          
	          <div class="white-panel col-lg-12 mt ">
	          	<ul class="nav nav-tabs mt-5 mb-4">

                  <li class="px-4">
                    <h4 >Subir Documento Financiero Periodo <h4/>
                  </li>
                </ul>
	            <div class="col-lg-6 text-left">
	              <form action="{{ route('admin.finance.upload_excel')}}" method="POST" enctype="multipart/form-data">
	              	@csrf
	              	<div class="form-group">
                        <label class="col-12">Periodo</label>
                        <div class="col-12">
                          	<select name="period_id" class="form-control @error('period_id') is-invalid @enderror" required>
								<option value="" disabled="" selected=""> Escoja el periodo </option>
								@foreach($periods as $period)
									
									<option value="{{ $period->id }}">{{ $period->year.'-'.$period->month }}</option>

								@endforeach
							</select>

							@error('period_id')
			                    <span class="invalid-feedback" role="alert">
			                        <strong>{{ $message }}</strong>
			                    </span>
			                @enderror
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="col-12">Centro de Producción</label>
                        <div class="col-12">
                          
                          	<select name="production_id" class="form-control @error('production_id') is-invalid @enderror" required>
								<option value="" disabled="" selected=""> Escoja un Centro </option>
								@foreach($productions as $production)
									@if($production->is_active($production) && $production->id!==1)
										<option value="{{ $production->id }}">{{ $production->name }}</option>
									@endif
								@endforeach
							</select>

							@error('production_id')
			                    <span class="invalid-feedback" role="alert">
			                        <strong>{{ $message }}</strong>
			                    </span>
			                @enderror

                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>

                      <div class="form-group">
                      	<div class="col-12">
                      		<input type="file" name="file" >
                      	</div>
		              	
                      	
                      </div>

                      <div class="form-group text-center">
                      	<button type="submit" class=" btn btn-theme">Cargar datos</button>		              	
                      </div>

	              	

	              </form>
	            </div>
	          </div>
	        </div>
    </section>
</section>




	


	
	

@endsection

@section('footer')
  <script src="{{asset('lib/dropzone/dropzone.js')}}"></script>


@endsection