@extends('admin.layouts.app')

@section('title','Editar Financiero '.$finance->code)

@section('header')
  <link href="{{asset('lib/dropzone/css/dropzone.css')}}" rel="stylesheet" />


@endsection
@section('document')
active
@endsection
@section('finance')
active
@endsection
@section('content')

		
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <ul class="nav nav-tabs ">

                  <li class="">
                    <h4 >Editar Documento Financiero Periodo {{ $finance->period->year.'-'.$finance->period->month }}<h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.finance.update',$finance) }}" >
                        	{{ method_field('PUT') }}
                        	@csrf
                        	<div class="col-lg-6 col-md-12  ">


	                          <div class="form-group">
	                            <label class="col-12"> Codigo</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ $finance->code }}"  autocomplete="code" autofocus placeholder=""> 
									@error('code')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12"> Fecha del recibo</label>
	                            <div class="col-12">
	                              	<input type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ $finance->date }}"  autocomplete="date" autofocus placeholder="00-00-0000"> 
									@error('date')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>

	                          <div class="form-group">
	                            <label class="col-12">Monto</label>
	                            <div class="col-12">
	                              	<input type="number" step="0.01" class="form-control @error('quantity') is-invalid @enderror" name="quantity" value="{{ $finance->quantity }}"  autocomplete="quantity" autofocus placeholder="000.00"> 
									@error('quantity')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12"> Observación</label>
	                            <div class="col-12">
	                              	<input type="text" class="form-control @error('observation') is-invalid @enderror" name="observation" value="{{ $finance->observation }}"  autocomplete="observation" autofocus placeholder=""> 
									@error('observation')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          <div class="form-group">
	                            <label class="col-12">Periodo</label>
	                            <div class="col-12">
	                              	<select name="period_id" class="form-control @error('period_id') is-invalid @enderror" required>
										<option value="{{ $finance->period->id }}"  selected=""> {{ $finance->period->year.'-'.$finance->period->month }} </option>
										@foreach($periods as $period)
											
											<option value="{{ $period->id }}">{{ $period->year.'-'.$period->month }}</option>

										@endforeach
									</select>

									@error('period_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror
	                            </div>
	                          </div>
	                          
	                          <div class="form-group">
	                            <label class="col-12">Centro de Producción</label>
	                            <div class="col-12">
	                              
	                              	<select name="production_id" class="form-control @error('production_id') is-invalid @enderror" required>
										<option value="{{ $finance->production->id }}" selected=""> {{ $finance->production->name }} </option>
										@foreach($productions as $production)
											@if($production->is_active($production) && $production->id!==1)
												<option value="{{ $production->id }}">{{ $production->name }}</option>
											@endif
										@endforeach
									</select>

									@error('production_id')
					                    <span class="invalid-feedback" role="alert">
					                        <strong>{{ $message }}</strong>
					                    </span>
					                @enderror

	                            </div>
	                          </div>
	                          
	                          
	                        </div>
	                        
	                        <div class="form-group">
	                            <div class="col-lg-offset-2 col-lg-10">
	                              <button class="btn btn-theme" type="submit">Guardar</button>
	                              <a class="btn btn-theme04" href="{{ URL::previous() }}">Cancel</a>
	                            </div>
	                          </div>
                        </form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
          
    </section>
</section>




	


	
	

@endsection

@section('footer')
  <script src="{{asset('lib/dropzone/dropzone.js')}}"></script>


@endsection