@extends('admin.layouts.app')

@section('title',$event->name)

@section('header')


@endsection
@section('event')
active
@endsection
@section('content')
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading w-100">
                <div class="nav nav-tabs ">

                  <div class="col-lg-10 col-md-9 col-8  my-3" >
                    <h4 class="text-truncate">Datos del Evento  <strong>{{ $event->name }}</strong><h4/> 

                  </div>
                  <div class=" my-auto col-lg-2 col-md-3 col-4 ">
                  	<div class="dropdown  float-right">
          					  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          					    ACCIONES
          					  </button>
          					  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          					    <a class="dropdown-item" href="{{ route('admin.event.edit',$event)}}">Editar</a>
          					    <a class="dropdown-item" href="">Desactivar</a>
          					    <form method="POST" action="{{ route('admin.event.destroy',$event)}}">
          					    	@csrf
          	    					{{ method_field('DELETE') }}
          	    					<button class="dropdown-item text-danger" type="submit" >Eliminar</button>
          					    </form>

          					  </div>
          					</div>
                  </div>
                </div>
          
              </div>

              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        

                        	<div class="col-lg-6 col-md-12  ">

                        		<h3>Datos  </h3>
                        		<table class="table  ">
                        			<tr class="">
		                                <td class="" style="width:130px">
		                                  Nombre :
		                                </td>
		                                <td class="">
		                                  {{ $event->name }}
		                                </td>     
		                            </tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Fecha :
              							</td>
              							<td class="">
              								{{ $event->limit.' '.$event->hour}}
              							</td>			
                					</tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Descripción :
              							</td>
              							<td class="">
              								{!! nl2br(e($event->description)) !!}
              							</td>			
                					</tr>
                					<tr class="">
              							<td class="" style="width:130px">
              								Centros de producciones :
              							</td>
              							<td class="">
              								@foreach($productions as $production)
												<a href="{{ route('admin.production.show',$production) }}">{{ $production->name }} </a> <br>
											@endforeach
              							</td>			
                					</tr>
		                    							
                				</table>
                        	</div>
		                    <div class="col-lg-6 col-md-12  ">
		                    	<h3>Poster</h3>
	                        	<div class="w-100">
	                        		<img src="{{  Storage::url($event->src) }}">
	                        	</div>
	                       </div>
        				<!-- /col-lg-8 -->
                      </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
               
              </div>
              <!-- /panel-body -->

            </div>
            <!-- /col-lg-12 -->
    </div>
          

  </section>


</section>


	<form method="Post" action="{{ route('admin.event.destroy', $event) }}" name="delete_form">
		@csrf
	    {{ method_field('DELETE') }}
		
	</form>
@endsection

@section('footer')
<script type="text/javascript">
	function enviar_formulario()	
	{
		swal({
			title: "¿quiere eliminar?",
			text: "Esta acción no se puede deshacer",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Sí, continuar",
			cancelButtonText: "No, Cancelar",
		
		}).then((result) => {
			if(result.value){
				document.delete_form.submit();
			}else{
				swal(
					'Operación cancelada',
					'Registro no eliminada',
					'error'
				)
			}
		});
	}
</script>

@endsection