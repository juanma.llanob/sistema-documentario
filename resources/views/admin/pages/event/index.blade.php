@extends('admin.layouts.app')

@section('title','Eventos')

@section('header')


@endsection
@section('event')
active
@endsection
@section('content')


	<section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row mt">
          <div class="col-sm-3">
            <section class="panel">
              <div class="panel-body">
              		<a href="{{ route('admin.event.create')}}" disable="" class="btn btn-compose text-truncate">
                  		<i class="fa fa-pencil"></i>  Agregar Evento
                	</a>


                <ul class="nav nav-pills nav-stacked mail-nav row">
                   <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Activos
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                  <li class="w-100">
                  	<a href="#"> 
                  		<i class="fa fa-user"></i> Vencidos
                  		<span class="label label-theme pull-right inbox-notification">3</span>
                  	</a>
                  </li>
                 
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-9">
            <section class="panel">
              <header class="panel-heading wht-bg">
                <h4 class="gen-case">
                    Lista de Eventos
                    <form action="#" class="pull-right mail-src-position">
                      <div class="input-append">
                        <input type="text" class="form-control " placeholder="Search Mail">
                      </div>
                    </form>
                  </h4>
              </header>
              <div class="panel-body minimal">
         
                <div class="table-inbox-wrap ">
                  <table class="table table-inbox table-hover">
                    <tbody>
                    	@foreach($events as $event)
                    	<tr>	
							<td class="inbox-small-cells">
								<input type="checkbox" class="mail-checkbox">
							</td>
							<td class="view-message "><a href="{{ route('admin.event.show', $event) }}">{{ $event->name }}</a></td>
							<td class="view-message ">
								<a href="{{ route('admin.event.show', $event) }}">
			                    	@if($event->status===0 )
			                    		<i class="fa fa-lock"></i> vencido
			                    	@elseif($event->status===1)
			                    		<i class="fa fa-unlock"></i> activo 
			                    	@endif	
								</a>
							</td>
							<td class="view-message "><a href="{{ route('admin.event.show', $event) }}">{{ $event->limit }}</a></td>
							<td class="view-message "><a href="{{ route('admin.event.edit', $event) }}" class="px-3"><i class="fa fa-pencil"></i></a></td>
							<td class="view-message  text-right">
								<a href="{{ route('admin.event.show', $event) }}" class="px-3">
									{{ $event->updated_at }}
								</a>
								</i></a>
							</td>
						</tr>
						@endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>


	
@endsection

@section('footer')


@endsection