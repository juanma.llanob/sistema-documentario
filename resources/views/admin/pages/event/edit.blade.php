@extends('admin.layouts.app')

@section('title','Editar '.$event->name)

@section('header')


@endsection
@section('event')
active
@endsection
@section('content')


<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading container">
                <ul class="nav border-bottom ">

                    <h4> Editar Evento : <h4/>
                  </li>
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body w-100">

                  <div id="registrar" class="">
                    <div class="col-12">
                        <form role="form"  class="form-horizontal" method="POST" action="{{ route('admin.event.update',$event) }}" enctype="multipart/form-data">
                        	@csrf
                        	{{ method_field('PUT') }}
                        	<div class="col-lg-6 col-md-12  ">

                        		<div class="form-group">
		                            <label class="col-12"> Nombre</label>
		                            <div class="col-12">
		                              	<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $event->name }}"  autocomplete="name" autofocus placeholder=""> 
										@error('name')
						                    <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                    </span>
						                @enderror
		                            </div>
		                        </div>
		                       	<div class="form-group">
	                            <label class="col-12"> Descripción</label>
		                            <div class="col-12">
		                              	<textarea type="text" class="form-control @error('description') is-invalid @enderror" name="description"  autocomplete="description" rows="10"> {{ $event->description }} </textarea>
										@error('description')
						                    <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                    </span>
						                @enderror
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <label class="col-12"> Fecha</label>
		                            <div class="col-12">
		                              	<input type="date" class="form-control @error('limit') is-invalid @enderror" name="limit" value="{{ $event->limit }}"  autocomplete="limit" autofocus placeholder=""> 
										@error('limit')
						                    <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                    </span>
						                @enderror
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <label class="col-12"> Hora </label>
		                            <div class="col-12">
		                              	<input type="time" class="form-control @error('hour') is-invalid @enderror" name="hour" value="{{ $event->hour }}"  autocomplete="hour" autofocus placeholder="00-00-0000"> 
										@error('hour')
						                    <span class="invalid-feedback" role="alert">
						                        <strong>{{ $message }}</strong>
						                    </span>
						                @enderror
		                            </div>
		                          </div>

		                         
		                           <div class="form-group">
		                            	<label class="col-12">Centro de Producción</label>
		                            	<div class="col-12">
		                              		<div class="row conteiner">
		                              		@foreach($productions as $production)		  
		                              			
		                              			<div class="col-lg-6 col-md-6">
		                              				                          	
			                                		<input type="checkbox" name="productions[]"  class="my-auto"  value="{{ $production->id }}"  @if($production->has_event($production->id)) checked @endif />
			                                		<label for="name" class="my-auto mx-2 text-trucate"> {{ $production->name }} </label>
		                              			</div>

		                            		@endforeach
		                            		</div>
		                            	</div>
		                           </div>

		                          
		                        </div>
		                        <div class="col-lg-6 col-md-12">
		                        	<div class="form-group">
			                            <label class="col-12">Imagen Actual</label>
			                            <div class="col-12">
			                              	<img src="{{Storage::url($event->src)}}">
			                            </div>
		                          	</div>
		                        	<div class="form-group">
			                            <label class="col-12">cambiar</label>
			                            <div class="col-12">
			                              	<input id="files" type="file"  class=" @error('src') is-invalid @enderror" name="src"  accept="image/png, image/jpeg"> 
											@error('src')
							                    <span class="invalid-feedback" role="alert">
							                        <strong>{{ $message }}</strong>
							                    </span>
							                @enderror
			                            </div>
		                          	</div>
		                          	    <output id="list" class="w-100">
		                          	    	
		                          	    </output>
		                          	    <output id="nombre" class="w-100">
		                          	    	
		                          	    </output>
	                       	 		</div>
	                        
		                        <div class="form-group">
		                            <div class="col-lg-offset-2 col-lg-10">
		                              	<button class="btn btn-theme" type="submit">Actualizar</button>
		                              	<a class="btn btn-theme04" href="{{  URL::previous() }}">Cancel</a>
		                            </div>
		                        </div>
                        	</form>
                                         
                      <!-- /col-lg-8 -->
                    </div>
                    <!-- /row -->
                  </div>
                  <!-- /tab-pane -->
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
    </section>
</section>
<script>

  function archivo(evt) {
      var files = evt.target.files; // FileList object
 
      // Obtenemos la imagen del campo "file".
      for (var i = 0, f; f = files[i]; i++) {
        //Solo admitimos imágenes.
        if (!f.type.match('image.*')) {
            continue;
        }
 
        var reader = new FileReader();
 
        reader.onload = (function(theFile) {
            return function(e) {
              // Insertamos la imagen
             document.getElementById("list").innerHTML = ['<img class="thumb w-100" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
             document.getElementById("nombre").innerHTML = ['<p>', escape(theFile.name), '"</p>'].join('');
            };
        })(f);
 
        reader.readAsDataURL(f);
      }
  }
 
  document.getElementById('files').addEventListener('change', archivo, false);


</script>

@endsection

@section('footer')


@endsection