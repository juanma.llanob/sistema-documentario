


 <aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <p class="centered"><a href="{{ route('admin.user.show',Auth::user()) }}"><img src="/storage/{{Auth::user()->src}}" class="img-circle" width="50" height="50" ></a></p>
      <h5 class="centered">{{ Auth::user()->name }}</h5>
      <li class="mt">
        <a class="@yield('home')" href="{{ route('admin.home') }}">
          <i class="fa fa-home"></i>
          <span>Inicio</span>
          </a>
      </li>
      <li>
        <a class="@yield('production')" href="{{ route('admin.production.index') }}">
          <i class="fa fa-building-o"></i>
          <span>Gestión Producción </span>
          </a>
      </li>
      <li class="sub-menu ">
        <a class="@yield('user')"  href="javascript:;" >
          <i class="fa fa-user"></i>
          <span>Gestion Usuarios</span>
          </a>
        <ul class="sub " >
          <li class="@yield('admin')"><a href="{{ route('admin.user.index') }}">Usuario</a></li>
          <li class="@yield('permission')"><a href="{{ route('admin.permission.index') }}">Permisos</a></li>
          <li class="@yield('role')"><a href="{{ route('admin.role.index') }}">Roles</a></li>
        </ul>
      </li>
      <li class="sub-menu">
        <a class="@yield('document')" href="javascript:;">
          <i class="fa fa-file-o"></i>
          <span>Gestión Documento</span>
          </a>
        <ul class="sub">
          <li class="@yield('finance')" ><a href="{{ route('admin.finance.index') }}">Doc. Financiero</a></li>
          <li class="@yield('technical')"><a href="{{ route('admin.technical.index') }}">Doc. Tecnicos</a></li>

        </ul>
      </li>
      <li>
        <a class="@yield('period')" href="{{ route('admin.period.index') }}">
          <i class="fa fa-calendar-o"></i>
          <span>Gestión Cronograma </span>
          </a>
      </li>
      <li>
        <a class="@yield('event')" href="{{ route('admin.event.index') }}">
          <i class="fa fa-calendar-check-o"></i>
          <span>Gestión Eventos </span>
          </a>
      </li>
      <li>
        <a class="@yield('history')" href="{{ route('admin.production.index') }}">
          <i class="fa fa-history"></i>
          <span>Historial </span>
          </a>
      </li>
    <!-- sidebar menu end-->
  </div>
</aside>

<!--  <script type="text/javascript">
    $(document).ready(function() {
      var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Welcome to Dashio!',
        // (string | mandatory) the text inside the notification
        text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Developed by <a href="http://alvarez.is" target="_blank" style="color:#4ECDC4">Alvarez.is</a>.',
        // (string | optional) the image to display on the left
        image: 'img/ui-sam.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        time: 8000,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
      });

      return false;
    });
  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>

 -->