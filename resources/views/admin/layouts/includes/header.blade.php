<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

<link href="{{ asset('css/zabuto_calendar.css') }}" rel="stylesheet">
<link href="{{ asset('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('lib/gritter/css/jquery.gritter.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">


<script type="text/javascript" src="{{ asset('lib/chart-master/Chart.js') }}"></script>




@yield('header')