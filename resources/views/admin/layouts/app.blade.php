<!DOCTYPE html>
<html lang="es">
<head>
	<title>@yield('title')</title>
	@include('admin.layouts.includes.header')
</head>
<body>
	<section id="container">
		@include('admin.layouts.includes.navbar')
		@include('admin.layouts.includes.sidebar')
		@yield('subtitle')
		@yield('content')
	</section>
	
	
		
	


	
	@include('admin.layouts.includes.footer')
</body>
</html>