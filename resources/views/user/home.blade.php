@extends('user.layouts.user')

@section('title','Inicio')

@section('content')
<div class="container gedf-wrapper">
        <div class="row">
            <div class="col-md-12 col-lg-3 mb-5">
                <div class="card">
                    <div class="card-body">
                        <div class="h5 text-truncate">{{ Auth::user()->production->name }} </div>
                        <img class="w-100 mb-2" src="{{ Storage::url(Auth::user()->production->src) }}">
                        <div class="h7 text-muted">Código : {{ Auth::user()->production->code }}</div>
                        <div class="h7 text-muted">Correo : {{ Auth::user()->production->email }}</div>
                        <div class="h7 text-muted">Teléfono :{{ Auth::user()->production->phone }}</div>
                    </div>
                    <ul class="list-group list-group-flush ">
                        <li class="list-group-item">
                            <div class="h6 text-muted">Encargado</div>
                            <div class="h5"><a href="" class="card-link">Juan Manuel</a></div>
                        </li>
                        <li class="list-group-item">
                            <div class="h6 text-muted">Usuarios</div>
                            <div class="h5">23</div>
                        </li>
                        <li class="list-group-item">
                            <a href="" class=" form-control btn btn-light"> ver perfil</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 mb-5 ">

                <!-- 
                <div class="card gedf-card">
                    
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make
                                    a publication</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="images-tab" data-toggle="tab" role="tab" aria-controls="images" aria-selected="false" href="#images">Images</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                <div class="form-group">
                                    <label class="sr-only" for="message">post</label>
                                    <textarea class="form-control" id="message" rows="3" placeholder="What are you thinking?"></textarea>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Upload image</label>
                                    </div>
                                </div>
                                <div class="py-4"></div>
                            </div>
                        </div>
                        <div class="btn-toolbar justify-content-between">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-primary">share</button>
                            </div>
                            <div class="btn-group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <i class="fa fa-globe"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> Public</a>
                                    <a class="dropdown-item" href="#"><i class="fa fa-users"></i> Friends</a>
                                    <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Just me</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                -->

                <!--- \\\\\\\Post-->
                @foreach($events as $event)
                <div class="card mb-2">

                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" height="45" src="{{ Storage::url(Auth::user()->production->src) }}" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h5 m-0">Oficina de Centro de Producción</div>
                                    <div class="h7 text-muted">Charlotte</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <div class="h6 dropdown-header">Configuration</div>
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Hide</a>
                                        <a class="dropdown-item" href="#">Report</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <img class="card-img-center" src="{{ Storage::url($event->src) }}">
                    <div class="card-body">
                        <div class="d-flex  mb-3">
                          <div class=""><i class="fa fa-calendar"></i> {{ $event->limit }}</div>
                          <div class="ml-auto text-muted h7 my-auto"><i class="fa fa-clock-o"></i> {{ $event->created_at->diffForHumans() }}</div>
                        </div>
                       
                        <a class="card-link" >
                            <h5 class="card-title">{{ $event->name }}</h5>
                        </a>
                        <p class="card-text">
                            {{ $event->description }}
                        </p>
                        <p class="card-text h7">
                            @foreach($event->productions as $production)
                                {{ '-'.$production->name }}
                            @endforeach
                        </p>
                    </div>

                    <!--
                    <div class="card-footer">
                        <a href="#" class="card-link"><i class="fa fa-gittip"></i> Like</a>
                        <a href="#" class="card-link"><i class="fa fa-comment"></i> Comment</a>
                        <a href="#" class="card-link"><i class="fa fa-mail-forward"></i> Share</a>
                    </div>
                    -->

                </div>
                @endforeach
                
            </div>
            <div class="col-md-12 col-lg-3 mb-5">
                @foreach($technicals as $technical)
                <div class="card mb-2">
                    <div class="card-body">
                        <div class="d-flex ">
                          <div class=" text-truncate"> <h6 class="">{{ $technical->name }}</h6></div>
                          <div class="ml-auto text-muted h7 text-truncate" ><i class="fa fa-clock-o"></i> 10min</div>
                        </div>
                        <p class="card-subtitle h7 text-silver ">{{ $technical->observation }}</p>
                        <p class="h7 text-success  "> {{ $technical->status }}</p>
                        

                        <div class="d-flex ">
                          <a href="{{ route('admin.technical.downloadPDF', $technical) }}"" class="btn btn-light btn-sm mr-2 text-truncate h7 px-2"><i class="fa fa-eye"></i> Ver</a>
                          <a href="#" class="ml-auto btn btn-secondary btn-sm text-truncate h7"><i class="fa fa-pencil"></i> Editar</a>
                        </div>

                    </div>
                </div>
                @endforeach
                
                
            </div>
        </div>
    
</div>

@endsection

    

@section('footer')


@endsection


