@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3  text-center py-auto">
            <div class="w-100 card py-3 px-3">
                <img src="{{ Storage::url(Auth::user()->production->src) }}" class="mx-auto img-fluid w-100"  alt="avatar">
                <h6 class="mt-2 text-truncate">{{ Auth::user()->production->name }} </h6>  
                <div class="col-md-12 mt-3">
                    <h5 class="mt-2"></span> Datos</h5>
                    <table class=" table-sm mx-auto h7">
                        <tbody>                                    
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Nombre:
                                </td>
                                <td class="text-left text-truncate">
                                     {{ Auth::user()->production->name }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Código:
                                </td>
                                <td class="text-left text-truncate ">
                                    {{ Auth::user()->production->code }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Teléfono:
                                </td>
                                <td class="text-left text-truncate ">
                                    {{ Auth::user()->production->phone }}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Correo:
                                </td>
                                <td class="text-left text-truncate ">
                                    {{ Auth::user()->production->email }}
                                </td>
                            </tr>
                            
                        </tbody>
                            

                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td class=" " >
                                    Descripción:
                                </td>
                                
                            </tr>
                            <tr>
                                
                                <td class="text-left h7">
                                    {{ Auth::user()->production->description }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="w-100 mt-5 mb-2">
                         <a href="" class="btn btn-light btn-sm form-control "><i class="fa fa-edit"></i> Editar</a>
                    </div>
                   
                </div>       
            </div>
        </div>
        <div class=" col-lg-9">    
            <h4 class="text-muted mt-2 ml-2"> Lista de Usuarios </h4>
            <div class="row container ">
                
                @foreach($users as $user)
                <div class="col-lg-4 card ">
                    <div class=" card-body">
                        <div class=" text-center">
                            <div> <img src="https://img.icons8.com/bubbles/100/000000/administrator-male.png" class="img-lg rounded-circle" alt="profile image">
                                <h4>Sam Disanjo</h4>
                                <p class="h7 text-primary"> Admin</p>
                            </div>
                            <div class="h7">
                                
                                <i class="fa fa-phone"> 674564574</i></br>
                                <i class="fa fa-vcard-o"></i> 65475754</br>
                                <i class="fa fa-envelope-o "></i> juanma.llanob@gmail.com
                            </div>                            
                        </div>
                    </div>
                </div>
                @endforeach
            
            </div>   
        </div>
    </div>
</div>


@endsection

    

@section('footer')


@endsection


