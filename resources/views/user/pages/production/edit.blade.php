@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3  text-center py-auto">
            <div class="w-100 card py-3 px-3">
                <img src="//placehold.it/150" class="mx-auto img-fluid w-100"  alt="avatar">
                <h6 class="mt-2 text-truncate">Centro de Producción </h6>  
                <div class="col-md-12 mt-3">
                    <h5 class="mt-2"></span> Datos</h5>
                    <table class=" table-sm mx-auto ">
                        <tbody>                                    
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Nombre:
                                </td>
                                <td class="text-left text-truncate">
                                     Llano Barsaya
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Código:
                                </td>
                                <td class="text-left text-truncate ">
                                    546754745
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Teléfono:
                                </td>
                                <td class="text-left text-truncate ">
                                    8767689999
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Correo:
                                </td>
                                <td class="text-left text-truncate ">
                                    juanma@gmail.com
                                </td>
                            </tr>
                            
                        </tbody>
                            

                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td class=" " >
                                    Descripción:
                                </td>
                                
                            </tr>
                            <tr>
                                
                                <td class="text-left h7">
                                    546754745dvdsgdsg sdgds gdf gdfhfgh fghfghn fg nhfgh fghfghfgh fg hfg hfg hfgdfsd dsfsdf sdf sdf sdf 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="w-100 mt-5 mb-2">
                         <a href="" class="btn btn-light btn-sm form-control "><i class="fa fa-edit"></i> Editar</a>
                    </div>
                   
                </div>       
            </div>
        </div>

        <div class="col-lg-9  ">
            <h4 class="text-muted mt-2 ml-2">Editar</h4>
            <div class="card ">
                
                <div class="card-body">
                    <form>
                      <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="inputAddress">Nombre</label>
                            <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Código</label>
                          <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Teléfono</label>
                          <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputAddress">Correo</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                      </div>
                      <div class="form-group">
                        <label for="inputAddress2">Descripción</label>
                        <textarea type="text" class="form-control" id="inputAddress2" rows="3" >
                        </textarea>
                      </div>
                      
                      <button type="submit" class="btn btn-primary">Actualizar</button>
                    </form>
                </div>
 
            
            </div>
        </div>
        
    </div>
    
</div>


@endsection

    

@section('footer')


@endsection


