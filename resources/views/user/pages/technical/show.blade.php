@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="{{ route('user.technical.index') }}" class="list-group-item list-group-item-action">Listar</a>
                  <a href="{{ route('user.technical.create') }}" class="list-group-item list-group-item-action">Agregar</a>
                  <a href="{{ route('user.technical.edit',$technical) }}" class="list-group-item list-group-item-action">Editar</a>
                  
                </div>
            </div>
        </div>
        <div class="col-sm-5  ">
            <h4 class="mt-2 ml-2 text-muted">Datos del Documento</h4>
            <div class="card ">
                <div class="tab-content card-body py-3">
                   <table class="table table-sm">
                   
                     <tbody class="h6">
                       <tr>
                         <td class="border-top-0">Título:</td>
                         <td class="border-top-0">{{ $technical->name }}</td>
                       </tr>
                       
                       <tr>
                         <td >Nº de oficio:</td>
                         <td >{{ $technical->code }}</td>
                       </tr>
                       <tr>
                         <td >Descripción</td>
                         <td >{{ $technical->observation }}</td>
                       </tr>
                       <tr>
                         <td >Tipo</td>
                         <td >{{ $technical->technicaltype->name }}</td>
                       </tr>
                       <tr>
                         <td >Estado:</td>
                         <td >{{ $technical->status }}</td>
                       </tr>
                       <tr>
                         <td >Descargar : </td>
                         <td><a class="link" href="{{ Storage::url($technical->document)}}"><i class="fa fa-file-pdf-o"></i> documento</a></td>
                       </tr>
                         
                       
                     </tbody>
                   </table>
                </div>
            
            </div>
        </div>
        <div class="col-sm-4 ">
            <h4 class="mt-2 ml-2 text-muted">Mensajes</h4>
            <div class="card ">
                <div class="card-header text-truncate"> 
                  O.F. Centro de producciones
                </div>
                <div class="tab-content card-body py-3">


                   <table class="table table">

                     <tbody class="h7">
                      @foreach($technical->technicalconditions as $message)
                        
                        @if ($message->user->id===Auth::user()->id)
                         <tr>
                           <td class="text-truncate bg-light border-right border-top-0">{{ $message->user->name }}</td>
                           <td class="border-top-0">{{ $message->message }}</td>
                         </tr>
                         @else
                         <tr>
                           <td class="text-truncate border-right border-top-0">{{ $message->user->name }}</td>
                           <td class="border-top-0">{{ $message->message }}</td>
                         </tr>
                         @endif
                      @endforeach
                       
                     </tbody>
                   </table>
                   @if($technical->status!=='aprobado')
                    <form method="POST" action="{{ route('user.technicalcondition.store',$technical) }}" >
                       @csrf
                       <input type="text" name="message" class="form-control" required="" placeholder="requiere mensaje"> 
                       <input type="text" hidden="" name="technical_id" value="{{ $technical->id}}" >
                       <button type="submit" class="btn btn-theme">Enviar</button>
                     </form>
                    @endif
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>

@endsection

    

@section('footer')


@endsection


