@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="{{ route('user.technical.index') }}" class="active list-group-item list-group-item-action">Listar</a>
                  <a href="{{ route('user.technical.create') }}" class="list-group-item list-group-item-action">Agregar</a>
                  
                </div>
            </div>
        </div>
        <div class="col-lg-9  ">
            <h4 class="mt-2 ml-2 text-muted">Lista Técnico</h4>
            <div class="card ">
                <div class="tab-content card-body py-3">
                   <table class="table table-sm">
                    <thead class="">
                      
                         <td class="border-top-0">Nº Experiente</td>
                         <td class="border-top-0">Nombre</td>
                         <td class="border-top-0">Tipo</td>
                         <td class="border-top-0 text-center">Archivo</td>
                         <td class="border-top-0 text-right">Opciones</td>
                      
                    </thead>
                     <tbody class="h7">
                      @foreach($technicals as $technical)
                       <tr>
                         <td>{{ $technical->code }}</td>
                         <td class="text-truncate">{{ $technical->name }}</td>
                         <td>{{ $technical->technicaltype->name }}</td>
                         <td class="text-center"><a class="text-dark" href="{{ Storage::url($technical->document) }}"><i class="fa fa-file-pdf-o"></i></a></td>
                         <td class="text-right">
                          <a href="{{ route('user.technical.show',$technical) }}" class="text-dark px-2"><i class="fa fa-eye"></i></a>
                           <a href="{{ route('user.technical.edit',$technical)}}" class="text-dark px-2"><i class="fa fa-edit"></i></a>
                           <a href="{{ route('user.technical.destroy',$technical) }}" class="text-dark px-2"><i class="fa fa-trash"></i></a>
                         </td>
                       </tr>
                      @endforeach
                       
                     </tbody>
                   </table>
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>

@endsection

    

@section('footer')


@endsection


