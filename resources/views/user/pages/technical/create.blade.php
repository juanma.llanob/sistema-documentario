@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="{{ route('user.technical.index') }}" class=" list-group-item list-group-item-action">Listar</a>
                  <a href="{{ route('user.technical.create') }}" class="active list-group-item list-group-item-action">Agregar</a>
                  
                </div>
            </div>
        </div>
        <div class="col-lg-9  ">
            <h4 class="mt-2 ml-2 text-muted">Agregar Documento Técnico</h4>
            <div class="card ">
                <div class="tab-content card-body py-4">
                    <form method="Post" action="{{ route('user.technical.store') }}" enctype="multipart/form-data">
                      @csrf
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">Código</label>
                          <input name="code" type="text" class="form-control"  placeholder="" required="">
                        </div>
                        <div class="form-group col-md-8">
                          <label for="inputPassword4">Nombre</label>
                          <input name="name" type="text" class="form-control"  placeholder="" required="">
                        </div>
                      
                        <div class="form-group col-md-12">
                          <label for="inputAddress2">Observación</label>
                          <textarea  name="observation" class="form-control" >
                          </textarea>    
                        </div>
                        <div class="form-row col-md-12">
                          <div class="form-group col-md-6">
                            <label for="inputCity">Tipo de documento</label>
                            <select class="form-control" name="technicaltype_id" >
                              @foreach($types as $type)
                                <option value="{{ $type->id}}">{{ $type->name}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputCity">Centro de Producción</label>
                            <div class="form-control"> {{ Auth::user()->production->name}}</div>
                            <input name="production_id" value="{{ Auth::user()->production->id }}" type="text" hidden="" class="form-control-plaintext" >
                          </div>
                          
                          
                        </div>
                        <div class="form-group col-md-12">
                          <label> Subir Documento</label>
                          <div class="custom-file">
                            <input name="document" type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>

                        </div>
                       
                      </div> 

                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <a href="{{ route('user.technical.index')}}" class="btn btn-secondary">Cancelar</a>
                    </form>
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

</script>
    
@endsection

@section('footer')


@endsection


