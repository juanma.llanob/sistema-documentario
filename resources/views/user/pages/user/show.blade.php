@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3  text-center py-auto">
            <div class="w-100 card py-5">
                <img src="/storage/{{Auth::user()->src}}" class="mx-auto img-fluid rounded-circle" width="150" height="150" alt="avatar">
                <h6 class="mt-2">{{ Auth::user()->name}} </h6>  
                <div class="col-md-12 mt-3">
                    <h5 class="mt-2"></span> Datos</h5>
                    <table class=" table-sm mx-auto ">
                        <tbody class="h7">                                    
                            <tr>
                                <td class="text-right text-muted" width="30%">
                                    Apellido:
                                </td>
                                <td class="text-left text-truncate">
                                     {{ Auth::user()->surname}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="30%">
                                    DNI:
                                </td>
                                <td class="text-left text-truncate ">
                                    {{ Auth::user()->dni}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="30%">
                                    Teléfono:
                                </td>
                                <td class="text-left text-truncate ">
                                    {{ Auth::user()->phone}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="30%">
                                    Correo:
                                </td>
                                <td class="text-left text-truncate ">
                                    {{Auth::user()->email}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="30%">
                                    Trabajo:
                                </td>
                                <td class="text-left text-truncate ">
                                    Secretario
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>       
            </div>
        </div>
        <div class="col-lg-9  ">
            <h4 class="mt-2 ml-2 text-muted">Editar</h4>
            <div class="card ">
                <div class="tab-content card-body py-4">
                        <form role="form">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Nombre</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" value="{{ Auth::user()->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Apellido</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" value="{{ Auth::user()->surname}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Correo</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="email" value="{{Auth::user()->email}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label" >Centro de producción</label>

                                <div class="col-lg-9">
                                    <input class="form-control-plaintext" readonly id="staticEmail2" type="text" value="{{Auth::user()->production->name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">DNI</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="url" value="{{ Auth::user()->dni}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Teléfono</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" value="{{ Auth::user()->phone}}" placeholder="Street">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Tipo de Trabajo</label>
                                <div class="col-lg-9">
                                    <select  class="form-control">
                                        <option value="{{Auth::user()->jobtype->id}}" disabled="" selected="">{{Auth::user()->jobtype->name}}</option>
                                        @foreach($jobtypes as $job)
                                            <option value="{{$job->id}}">{{$job->name}}</option>
                                        @endforeach
                                       
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Password</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                <div class="col-lg-9">
                                    <input type="reset" class="btn btn-secondary" value="Cancel">
                                    <input type="button" class="btn btn-primary" value="Actualizar">
                                </div>
                            </div>
                        </form>
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>

@endsection

    

@section('footer')


@endsection


