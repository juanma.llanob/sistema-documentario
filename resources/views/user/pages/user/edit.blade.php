@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3  text-center py-auto">
            <div class="w-100 card py-5">
                <img src="//placehold.it/150" class="mx-auto img-fluid rounded-circle" width="150" height="150" alt="avatar">
                <h6 class="mt-2">Juan Manuel </h6>  
                <div class="col-md-12 mt-3">
                    <h5 class="mt-2"></span> Datos</h5>
                    <table class=" table-sm mx-auto ">
                        <tbody>                                    
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Apellido :
                                </td>
                                <td class="text-left text-truncate">
                                     Llano Barsaya
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    DNI :
                                </td>
                                <td class="text-left text-truncate ">
                                    546754745
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Teléfono :
                                </td>
                                <td class="text-left text-truncate ">
                                    8767689999
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Correo :
                                </td>
                                <td class="text-left text-truncate ">
                                    juanma@gmail.com
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right text-muted" width="50%">
                                    Trabajo :
                                </td>
                                <td class="text-left text-truncate ">
                                    Secretario
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>       
            </div>
        </div>
        <div class="col-lg-9  ">
            <div class="card ">
                <ul class="nav nav-tabs card-header">
                    <li class="nav-item">
                        <a href="" data-target="#profile" data-toggle="tab" class="card-link ">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a href="" data-target="#messages" data-toggle="tab" class=" card-link">Actividades</a>
                    </li>
                    <li class="nav-item">
                        <a href="" data-target="#edit" data-toggle="tab" class="card-link">Edit</a>
                    </li>
                </ul>
                <div class="tab-content card-body py-4">
                    <div class="tab-pane " id="profile">
                        <h5 class="mb-3">User Profile</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <h6>About</h6>
                                <p>
                                    Web Designer, UI/UX Engineer
                                </p>
                                <h6>Hobbies</h6>
                                <p>
                                    Indie music, skiing and hiking. I love the great outdoors.
                                </p>
                            </div>
                            
                            <div class="col-md-12">
                                <h5 class="mt-2"></span> Recent Activity</h5>
                                <table class="table table-sm table-hover ">
                                    <tbody>                                    
                                        <tr>
                                            <td>
                                                <strong>Abby</strong> joined ACME Project Team in <strong>`Collaboration`</strong>
                                            </td>
                                            <td class="text-right text-muted h7">
                                                10min
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Gary</strong> deleted My Board1 in <strong>`Discussions`</strong>
                                            </td>
                                            <td class="text-right text-muted h7">
                                                10min
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Kensington</strong> deleted MyBoard3 in <strong>`Discussions`</strong>
                                            </td>
                                            <td class="text-right text-muted h7">
                                                10min
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>John</strong> deleted My Board1 in <strong>`Discussions`</strong>
                                            </td>
                                            <td class="text-right text-muted h7">
                                                10min
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Skell</strong> deleted his post Look at Why this is.. in <strong>`Discussions`</strong>
                                            </td>
                                            <td class="text-right text-muted h7">
                                                10min
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--/row-->
                    </div>
                    <div class="tab-pane" id="messages">
                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use this to show important messages to the user.
                        </div>
                        <table class="table table-hover table-striped">
                            <tbody>                                    
                                <tr>
                                    <td>
                                       <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus. 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros. 
                                    </td>
                                </tr>
                            </tbody> 
                        </table>
                    </div>
                    <div class="tab-pane" id="edit">
                        <form role="form">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Nombre</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" value="Jane">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Apellido</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" value="Bishop">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Correo</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="email" value="email@gmail.com">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label" >Centro de producción</label>

                                <div class="col-lg-9">
                                    <input class="form-control-plaintext" readonly id="staticEmail2" type="text" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">DNI</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="url" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Teléfono</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="text" value="" placeholder="Street">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Tipo de Trabajo</label>
                                <div class="col-lg-9">
                                    <select id="user_time_zone" class="form-control">
                                        <option value="1">sfsdfsdf</option>

                                        <option value="1">sfsdfsdf</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Password</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="password" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                <div class="col-lg-9">
                                    <input class="form-control" type="password" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label"></label>
                                <div class="col-lg-9">
                                    <input type="reset" class="btn btn-secondary" value="Cancel">
                                    <input type="button" class="btn btn-primary" value="Save Changes">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>

@endsection

    

@section('footer')


@endsection


