@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="#" class="list-group-item list-group-item-action">Lista Actual</a>
                  <a href="#" class="list-group-item list-group-item-action">Agregar</a>
                  <a href="#" class="list-group-item list-group-item-action">Valor Mensual</a>
                  <a href="#" class="list-group-item list-group-item-action disabled">Valor Anual</a>
                </div>
            </div>
        </div>
        <div class="col-lg-9  ">
            <h4 class="mt-2 ml-2 text-muted">Lista Financiero Mensual</h4>
            <div class="container row">
                <a class="card col-6 col-md-4 col-lg-3 bg-white rounded text-center pt-3 pb-2 card-link text-dark"  href="">
                    <h6>Financiero 2020</h6>
                    <p class="text-muted mt-2 h6">monto</p>
                    <h5>43534.64</h5>
                  
                </a>
                <a class="card col-6 col-md-4 col-lg-3 bg-white rounded text-center pt-3 pb-2">
                    <h6>Financiero 2019</h6>
                    <p class="text-muted mt-2 h6">monto</p>
                    <h5>43534.64</h5>
                  
                </a>
               
            
            </div>
        </div>
        
    </div>
    
</div>

@endsection

    

@section('footer')


@endsection


