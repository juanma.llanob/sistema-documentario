@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="{{ route('user.finance.index') }}" class=" list-group-item list-group-item-action">Listar</a>
                  <a href="{{ route('user.finance.create') }}" class="active list-group-item list-group-item-action">Agregar</a>
                </div>
            </div>
        </div>
        <div class="col-lg-9  ">
            <h4 class="mt-2 ml-2 text-muted">Agregar Estado financiero</h4>
            <div class="card ">
                <div class="tab-content card-body py-4">
                    <form method="POST" action="{{ route('user.finance.store')}}">
                      @csrf
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">Código</label>
                          <input type="number" class="form-control" name="code"  required="">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Fecha</label>
                          <input type="date" class="form-control" name="date" id="inputPassword4"  required="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Cantidad</label>
                            <input type="number" class="form-control" name="quantity" id="inputAddress" placeholder="0000.00" step="0.01" required="">
                          </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="inputAddress2">Observación</label>
                        <textarea name="observation" class="form-control" id="inputAddress2">
                        </textarea>    
                      </div>
                      <div class="form-row">
                        <div class="form-group col-md-8">
                          <label for="inputCity">Centro de Producción</label>
                          <div class="form-control">{{ Auth::user()->production->name }}</div>
                          <input type="text" name="production_id" value="{{ Auth::user()->production->id }}" hidden="" class="form-control-plaintext" id="inputCity">
                        </div>
                        
                        <div class="form-group col-md-4">
                          <label for="inputZip">Periodo Actual</label>
                          <div class="form-control">{{ $period->year.' '.$period->month}}</div>
                          <input type="text" name="period_id" value="{{ $period->id }}" hidden="" class="form-control-plaintext" id="inputZip">
                        </div>
                      </div>
                      
                      <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="reset" class="btn btn-secondary">Cancelar</button>

                    </form>
                </div>
            
            </div>
        </div>
        <div class="col-md-3 py-auto">
        </div>
        <div class="col-md-9  ">
            <h4 class="mt-2 ml-2 text-muted">Subir Excel</h4>
            <div class="card ">
                <div class="tab-content card-body py-4">
                    <form action="{{ route('user.finance.upload_excel')}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      
                      <div class="form-row">
                        <div class="form-group col-md-8">
                          <label for="inputCity">Centro de Producción</label>
                          <div class="form-control">{{ Auth::user()->production->name }}</div>
                          <input type="text" name="production_id" value="{{ Auth::user()->production->id }}" hidden="" class="form-control-plaintext" id="inputCity">
                        </div>
                        
                        <div class="form-group col-md-4">
                          <label for="inputZip">Periodo Actual</label>
                          <div class="form-control">{{ $period->year.' '.$period->month}}</div>
                          <input type="text" name="period_id" value="{{ $period->id }}" hidden="" class="form-control-plaintext" id="inputZip">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputZip">Cargar Excel</label>
                          <div class="custom-file">
                            <input name="file" type="file" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">Excel</label>
                          </div>
                        </div>
                      </div>
                      
                      <button type="submit" class="btn btn-primary">Cargar</button>
                      <button type="reset" class="btn btn-secondary">Cancelar</button>

                    </form>
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

</script>


@endsection

    

@section('footer')


@endsection


