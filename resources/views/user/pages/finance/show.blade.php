@extends('user.layouts.user')

@section('title','Usuario')

@section('content')



<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="{{ route('user.finance.index') }}" class="list-group-item list-group-item-action">Listar</a>
                  <a href="{{ route('user.finance.create') }}" class="list-group-item list-group-item-action">Agregar</a>
                  <a href="#" class=" list-group-item list-group-item-action">Valor Mensual</a>
                  <a href="#" class="list-group-item list-group-item-action disabled">Valor Anual</a>
                </div>
            </div>
        </div>
        <div class="col-sm-5  ">
            <h4 class="mt-2 ml-2 text-muted">Datos del Documento</h4>
            <div class="card ">
                <div class="tab-content card-body py-3">
                  <table class="table table-sm">
                    <thead class="">
                      
                         <td class="border-top-0">Código</td>
                         <td class="border-top-0">Observación</td>
                         <td class="border-top-0">Fecha</td>
                         <td class="border-top-0">Monto</td>
                         <td class="border-top-0 text-right">Opciones</td>
                      
                    </thead>
                    <tbody class="h7">
                        @foreach($finances as $finance)
                        <tr>
                         <td>{{ $finance->code }}</td>
                         <td>{{ $finance->observation }}</td>
                         <td>{{ $finance->date }}</td>
                         <td>{{ $finance->quantity }}</td>
                         <td class="text-right">
                           <a href="" class="text-dark px-2"><i class="fa fa-edit"></i></a>
                           <a href="" class="text-dark px-2"><i class="fa fa-trash"></i></a>
                         </td>
                        </tr>
                       @endforeach
                     </tbody>
                   </table>
                </div>
            
            </div>
        </div>
        

        
    </div>
    
</div>


@endsection

    

@section('footer')


@endsection
