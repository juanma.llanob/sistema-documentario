@extends('user.layouts.user')

@section('title','Usuario')

@section('content')
<div class="container gedf-wrapper ">
    <div class="row my-2">
        <div class="col-lg-3 py-auto">
            <div class=" py-2 px-2">
                    <a class="h4 text-muted card-link" href="">Centro de Producción </a>  
                </div>
            <div class="w-100 card">
                
                <div class="list-group">
                  <a href="{{ route('user.finance.index') }}" class="list-group-item list-group-item-action">Listar</a>
                  <a href="{{ route('user.finance.create') }}" class="list-group-item list-group-item-action">Agregar</a>
                  <a href="{{ route('user.finance.edit',$finance) }}" class="active list-group-item list-group-item-action">Edit</a>
                  <a href="#" class="list-group-item list-group-item-action">Valor Mensual</a>
                  <a href="#" class="list-group-item list-group-item-action disabled">Valor Anual</a>
                </div>
            </div>
        </div>
        <div class="col-lg-9  ">
            <h4 class="mt-2 ml-2 text-muted">Editar Estado financiero DFR533</h4>
            <div class="card ">
                <div class="tab-content card-body py-4">
                    <form>
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <label for="inputEmail4">Código</label>
                          <input type="number" class="form-control" id="inputEmail4" placeholder="Email" required="">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputPassword4">Fecha</label>
                          <input type="date" class="form-control" id="inputPassword4" placeholder="Password" required="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputAddress">Cantidad</label>
                            <input type="number" class="form-control" id="inputAddress" placeholder="0000.00" required="">
                          </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="inputAddress2">Observación</label>
                        <textarea type="text" class="form-control" id="inputAddress2">
                        </textarea>    
                      </div>
                      <div class="form-row">
                        <div class="form-group col-md-8">
                          <label for="inputCity">Centro de Producción</label>
                          <input type="text" readonly class="form-control-plaintext" id="inputCity">
                        </div>
                        
                        <div class="form-group col-md-4">
                          <label for="inputZip">Periodo Actual</label>
                          <input type="text" readonly class="form-control-plaintext" id="inputZip">
                        </div>
                      </div>
                      
                      <button type="submit" class="btn btn-primary">Guardar</button>
                      <button type="reset" class="btn btn-secondary">Restablecer</button>
                    </form>
                </div>
            
            </div>
        </div>
        
    </div>
    
</div>

@endsection

    

@section('footer')


@endsection


