<!DOCTYPE html>
<html lang="es">
<head>
	<title>@yield('title')</title>
	@include('user.layouts.includes.header')
</head>
<body>
	@include('user.layouts.includes.navbar')
	@yield('content')	
	@include('user.layouts.includes.footer')
</body>
</html>