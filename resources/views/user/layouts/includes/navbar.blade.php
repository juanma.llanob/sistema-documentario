    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
        <div class="container py-1">
            <a class="navbar-brand my-auto" href="{{ route('user.inicio') }}">
                <p class="h4 mt-2 pt-1"> O.C.PRODUCCION </p>
            </a>
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse " id="navbarResponsive">
                <ul class="navbar-nav ml-auto ">
                    @if(Auth::user()->is_admin(Auth::user()->roles))   
                        <li class="nav-item mx-4 my-auto">
                            <a class="nav-link" href="{{ route('admin.home') }}">Modo Admin</a>
                        </li>
                    @endif
                    <li class="nav-item active mx-4 my-auto">
                        <a class="nav-link" href="{{ route('user.inicio') }}">Inicio
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item mx-4 my-auto">
                        <a class="nav-link" href="">C.Producción</a>
                    </li>
                    <li class="nav-item dropdown mx-4 my-auto">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Documentos <i class="fa fa-angle-down ml-2"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('user.finance.index') }}">Financiero</a>
                            <a class="dropdown-item" href="{{ route('user.technical.index') }}">Técnico</a>
                        </div>
                    </li >
                    <li class="nav-item dropdown ml-4">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ Storage::url(Auth::user()->src)  }}" class="rounded-circle mr-2" width="30" height="30">
                            Juan Manuel <i class="fa fa-angle-down ml-2"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="">Perfil</a>
                            <a class="dropdown-item text-danger" ref="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Salir</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                              @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>