<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Production extends Model
{
    protected $fillable = [
        'code','name','src','description','email','phone',
    ];
    //Relaciones
    public function events(){
        return $this->belongsToMany('App\Event')->withTimestamps();
    }
    public function users()
    {
        return $this->hasMany('App\User');
    }
    public function technicals(){
        return $this->hasMany('App\Technical');
    }
    public function finances()
    {
        return $this->hasMany('App\Finance');
    }




    //Almacenamiento 
    public function store($request)
    {
        $production= Production::create($request->all());
        if($request->hasFile('src'))
        {
            $ruta=($request->file('src')->store('production','public'));
            $production->update(['src' => $ruta]);
        }

        alert('Exito','Centro de Production se ha guardado','success')->showConfirmButton();
        return $production;
    }
    public function my_update($request)
    {
        $production=User::findOrFail($request);
        $ruta= $this->src;
        if ($request->hasFile('src')) 
        {
            if ($this->src) 
            {
                Storage::delete('public/'.$this->src);
            }
                
            $ruta=$request->file('src')->store('production','public');
        }
   
        $production= Production::update([
            'name' => $request['name'],
            'code' => $request['code'],
            'src' => $ruta,
            'description' => $request['description'],
            'email' => $request['email'],
            'phone' => $request['phone'],
        
        ]);

        alert('Exito','Centro de Production se ha actualizado','success')->showConfirmButton();
        return $production;
    }
    //Validaciones

    public function has_event($id)
    {    
        foreach ($this->events as $event) {
            if($event->id == $id) return true;

        }
        return false;
    }

    public function is_active($production){
            if ($production->status==1) return true;
    }

    //Recuperación de información

    //Otras operaciones

    public static function production_month_total($production_id)
    {
        $period = Period::current_period();
        $filtrar=['period_id'=>$period->id,'production_id'=>$production_id];
        $total = Finance::where($filtrar)->sum('quantity');
        return $total;

    }

    public function has_finance_inPeriod($id)
    {

        foreach ($this->finances as $finance) {
            if($finance->production->id == $id && $finance->period->status==1) return true;

        }
        return false;
    }



    // Query Scope

    public function scopeStatus($query,$status){
      if($status)
        return $query->where('status',$status);
    }

}



