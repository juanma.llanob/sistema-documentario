<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Event extends Model
{
    protected $fillable = [
        'name','description','src','hour','limit',
    ];

    //Relaciones

    public function productions()
    {
    	return $this->belongsToMany('App\Production')->withTimestamps();
    }

    //Almacenamiento
    public function store($request){
        $event =Event::create($request->all());
        if($request->hasFile('src'))
        {
            $ruta=($request->file('src')->store('event','public'));
            $event->update(['src' => $ruta]);
        }
        alert('Exito','El Evento se ha guardado','success')->showConfirmButton();
    	return $event;
    }
    public function my_update($request){
        $ruta='';
        if ($request->hasFile('src')) 
        {
            if ($this->src) 
            {
                Storage::delete('public/'.$this->src);
            }
                
            $ruta=$request->file('src')->store('event','public');
        }
        $event= Event::update([
            'name' => $request['name'],
            'description' => $request['description'],
            'src' => $ruta,
            'hour' => $request['hour'],
            'limit' => $request['limit'],
        
        ]);
        alert('Exito','El Evento se ha actualizado','success')->showConfirmButton();

        return $event;
    }



    //otro 

    public function has_production($id){
        foreach ($this->productions as $production) {
            if ($production->id ==$id) {
                  return true;
              }  
        }
        return false;
    }


    
}
