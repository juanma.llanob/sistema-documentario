<?php

namespace App\Imports;

use App\Finance;
use Maatwebsite\Excel\Concerns\ToModel;
use Auth;
use App\Period;
class FinancesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $period=Period::current_period();
        return new Finance([

            'code'          => $row[0],
            'date'          => $row[1],
            'quantity'      => $row[2],
            'observation'   => $row[3],
            'period_id'     => $period->id,
            'production_id' => Auth::user()->production_id,

        ]);
    }
}
