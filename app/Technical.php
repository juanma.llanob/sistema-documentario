<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Auth;
class Technical extends Model
{
    protected $fillable = [
        'code','name','document','observation','technicaltype_id','production_id',
    ];
    //Relaciones
    public function technicaltype()
    {
    	return $this->belongsTo('App\Technicaltype');
    }
    public function technicalconditions()
    {
        return $this->hasMany('App\Technicalcondition');
    }
    public function production()
    {
    	return $this->belongsTo('App\Production');
    }

    //Almacenamiento

    public function store($request)
    {
        $technical= Technical::create($request->all());
        if($request->hasFile('document'))
        {
            $ruta=($request->file('document')->store('technical','public'));
            $technical->update(['document' => $ruta]);
        }

        alert('Exito','Oficio enviado','success')->showConfirmButton();
        return $technical;
        
    }
    public function my_update($request){
        $ruta='';
        if ($request->hasFile('document')) 
        {
            if ($this->document) 
            {
                Storage::delete('public/'.$this->document);
            }
                
            $ruta=$request->file('document')->store('technical','public');
        }
        $technical= Technical::update([
            'code' => $request['code'],
            'name' => $request['name'],
            'document' => $ruta,
            'observation' => $request['observation'],
            'technicaltype_id' => $request['technicaltype_id'],
            'production_id' => $request['production_id'],
        
        ]);
        alert('Exito','El Technical se ha actualizado','success')->showConfirmButton();

        return $technical;
    }
}
