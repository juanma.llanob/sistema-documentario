<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobtype extends Model
{
    protected $fillable = [
        'name','description',
    ];
    //Relaciones
    public function users()
    {
        return $this->hasMany('App\User');
    }

    //Almacenamiento 

    //Validaciones

    //Recuperación de información

    //Otras operaciones
}
