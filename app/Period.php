<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable = [
        'year','month','status','limiter',
    ];
    //Relaciones
    public function finances()
    {  
        return $this->hasMany('App\Finance');
    }

    //Almacenamiento 

    public function store($request){
        alert('Exito','El Periodo se ha guardado','success')->showConfirmButton();

        return Period::create($request->all());
    }

    public function my_update($request)
    {
        Period::update($request->all());
    }

    //Validaciones

    public function exists($period){

        if ($period->year==date('Y') && $period->month==date('n')) return true;
    }

    
    public static function current_period(){
        $active=Period::where('status',1)->first();
        return $active;
    }

    public function is_active($period){
        if ($period->status == 1 ) return true;
    }

    //Recuperación de información


    //Otras operaciones

    public static function change_status(){
        if (Period::where('status',1)->get()) {
            Period::where('status',1)->update(['status'=>0]);
        }
        
    }
    public static function limiter_period(){
        if (count(Period::where('status',1)->get())>0) {
            $period=Period::where('status',1)->first();
            if ($period->limiter < now()->toDateString()) {
                $period->update(['status'=>0]);
            }
        }
    }

}
