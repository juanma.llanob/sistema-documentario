<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Technicalcondition extends Model
{
    protected $fillable = [
        'message','technical_id','user_id'
    ];


    //relaciones
    public function technical()
    {
        return $this->belongsTo('App\Technical');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //almacenmaiento
    public static function store($technical_id,$user_id,$message){
    	Technicalcondition::create([
            'message' => $message,
            'technical_id' => $technical_id,
            'user_id' => $user_id,
        ]);
    }

}
