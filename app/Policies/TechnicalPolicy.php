<?php

namespace App\Policies;

use App\User;
use App\Technical;
use Illuminate\Auth\Access\HandlesAuthorization;

class TechnicalPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any technicals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the technical.
     *
     * @param  \App\User  $user
     * @param  \App\Technical  $technical
     * @return mixed
     */
    public function view(User $user, Technical $technical)
    {
        //
    }

    /**
     * Determine whether the user can create technicals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the technical.
     *
     * @param  \App\User  $user
     * @param  \App\Technical  $technical
     * @return mixed
     */
    public function update(User $user, Technical $technical)
    {
        //
    }

    /**
     * Determine whether the user can delete the technical.
     *
     * @param  \App\User  $user
     * @param  \App\Technical  $technical
     * @return mixed
     */
    public function delete(User $user, Technical $technical)
    {
        //
    }

    /**
     * Determine whether the user can restore the technical.
     *
     * @param  \App\User  $user
     * @param  \App\Technical  $technical
     * @return mixed
     */
    public function restore(User $user, Technical $technical)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the technical.
     *
     * @param  \App\User  $user
     * @param  \App\Technical  $technical
     * @return mixed
     */
    public function forceDelete(User $user, Technical $technical)
    {
        //
    }
}
