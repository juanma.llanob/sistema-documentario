<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technicaltype extends Model
{
    protected $fillable = [
        'name','description',
    ];

    //Relaciones
    public function technicals()
    {
    	return $this->hasMany('App\Technical');
    }

}
