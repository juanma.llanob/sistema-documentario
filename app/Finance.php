<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

 $production;
 $period;

class Finance extends Model
{
    protected $fillable = [
        'code','date','quantity','observation','period_id','production_id',
    ];
    protected $dates =[
        'date'
    ];

    //Relaciones 
    public function period()
    {
    	return $this->belongsTo('App\Period');
    }
    public function production()
    {
    	return $this->belongsTo('App\Production');
    }

    //Almacenamiento
    public function store($request)
    {
        alert('Exito','Estado Financiero se ha guardado','success')->showConfirmButton();
        return Finance::create($request->all());
    }
    //Otros 
    public static function production_month_total($period,$production_id)
    {
        $filtrar=['period_id'=>$period->id,'production_id'=>$production_id];
        $total = Finance::where($filtrar)->sum('quantity');
        return $total;

    }
    public static function global_variables($production_id,$period_id)
    {

        $production=$production_id;
        $period=$period_id;
        return true;
    }
    public static function return_variables()
    {

        $variable[0]=1;
        $variable[1]=1;
        return $variable;
    }

    // Query Scope
    
}





