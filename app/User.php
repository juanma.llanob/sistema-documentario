<?php

namespace App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surname','src','dni','phone','status','jobtype_id','production_id','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function jobtype()
    {
        return $this->belongsTo('App\Jobtype');
    } 

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function production()
    {
        return $this->belongsTo('App\Production');
    } 
    
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }
    public function technicalconditions()
    {
        return $this->hasMany('App\Technicalcondition');
    }

    //Almacenamiento 

    public function my_update($request)
    {   
        $user=User::findOrFail($request);
        $ruta= $this->src;
        if ($request->hasFile('src')) 
        {
            if ($this->src) 
            {
                Storage::delete('public/'.$this->src);
            }
                
            $ruta=$request->file('src')->store('user','public');
        }
   
        $user= User::update([
            'name' => $request['name'],
            'surname' => $request['surname'],
            'dni' => $request['dni'],
            'src' => $ruta,
            'phone' => $request['phone'],
            'email' => $request['email'],
            'jobtype_id' => $request['jobtype_id'],
            'production_id' => $request['production_id'],
            'password' => Hash::make($request['password']),
        ]);

        alert('Exito','Usuario ha sido actualizado ','success')->showConfirmButton();

        return $user;
    }

    public function store( $request )
    {   
        
        $user= User::create($request->all());
        if($request->hasFile('src'))
        {
            $ruta=($request->file('src')->store('user','public'));
            $user->update(['src' => $ruta]);
        }
        $user->update(['password' => Hash::make($request->password)]);

        $roles = [$request->role];
        $user->role_assignment(null,$roles);
        alert('Exito','Usuario ha sido creado ','success')->showConfirmButton();

        return $user;
    }

    public function role_assignment($request, array $roles = null)
    {
        $roles = (is_null($roles)) ? $request->roles : $roles;

        $this->permission_mass_assignment($roles);
        $this->roles()->sync($roles);
        $this->verify_permission_integrity($roles);
    } 

    //Validaciones
    public function is_admin()
    {   
        $admin=explode('-', 'Administrador');
        if ($this->has_role($admin)) {
            return true;
        }
        else{
            return false;
        }
    }

    public function has_any_role(array $roles)
    {
        foreach ($roles as $rol) 
        {
            if($this->has_role($rol))
                return true; 
        }
        return false;
    }

    public function has_role($role)
    {

        if($this->roles()->where('name',$role)->first())
        {
            return true;
        }
        return false;
    }

    



    public function has_permission($id)
    {
        foreach ($this->permissions as $permission) {
            if($permission->id == $id || $permission->slug == $id) return true;

        }
        return false;
    }
    //Recuperación de información

    //Otras operaciones

    public function verify_permission_integrity(array $roles)
    {
        $permissions=$this->permissions;
        foreach ($permissions as $permission) {
            if (!in_array($permission->role->id, $roles)) {
                $this->permissions()->detach($permission->id);
            }
        }
    }

    public function permission_mass_assignment(array $roles)
    {
        foreach ($roles as $role) {
            if (!$this->has_role($role)) {
                $role_obj=\App\Role::findOrFail($role);
                $permissions=$role_obj->permissions;
                $this->permissions()->syncWithoutDetaching($permissions);
            }
        }
    }
}




