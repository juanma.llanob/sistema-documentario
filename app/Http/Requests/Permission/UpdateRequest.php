<?php

namespace App\Http\Requests\Permission;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:permissions,name,'.$this->route('permission')->id.'|max:50',
            'description' => 'required|max:10000',
            'role_id' => 'required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El campo de nombre es requerido',
            'name.unique' => 'El campo ya esta usado',
            'description.required' => 'El campo es requerido',
            'role_id.required' => 'Rol es requerido',
            'role_id.numeric' => 'El formato no es correcto',

        ];
    }
}
