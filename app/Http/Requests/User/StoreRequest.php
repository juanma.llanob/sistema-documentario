<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'surname' => 'required|string|max:100',
            'src' => 'image',
            'dni' => 'required|numeric|unique:users',
            'phone' => 'required|numeric|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'jobtype_id' => 'required|numeric',
            'production_id' => 'required|numeric',
            'password' => 'required|string|min:8|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El campo es requerido',
            'name.string' => 'El campo es inválido',
            'name.max' => 'Solo es permitido 100 caracteres',
            'surname.required' => 'El campo es requerido',
            'surname.string' => 'El campo es inválido',
            'surname.max' => 'Solo es permitido 100 caracteres',
            'src.image' => 'Ingrese una imagen',
            'dni.required' => 'El campo es requerido',
            'dni.numeric' => 'Los datos no son válidos',
            'dni.unique' => 'El DNI ya existe',
            'phone.required' => 'El campo es requerido',
            'phone.numeric' => 'Los datos no son válidos',
            'phone.unique' => 'El teléfono ya existe',
            'email.required' => 'El campo es requerido',
            'email.string' => 'Los datos no son válidos',
            'email.email' => 'El formato no es correcto',
            'email.max' => 'Solo se permitido 255 caracteres',
            'email.unique' => 'EL correo ya existe',
            'jobtype_id.required' => 'Selecione una opción',
            'jobtype_id.numeric' => 'Campo incorrecto',
            'production_id.required' => 'Selecione una opción',
            'production_id.numeric' => 'Campo incorrecto',
            'password.required' => 'El campo es requerido',
            'password.string' => 'El campo es incorrecto',
            'password.min' => 'Tu contraseña debe tener almenos 8 caracteres',
            'password.confirmed' => 'Las ocntraseñas no coinciden',

        ];
    }
}
