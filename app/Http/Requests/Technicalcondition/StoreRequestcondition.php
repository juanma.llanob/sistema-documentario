<?php

namespace App\Http\Requests\Technicalcondition;
use App\Technicalcondition;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequestcondition extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'messages' => 'required|unique:technicals|max:50',

        ];
    }
    public function messages()
    {
        return [
            'code.required' => 'El campo es requerido',
        ];
    }
}
