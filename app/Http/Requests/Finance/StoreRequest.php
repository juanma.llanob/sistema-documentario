<?php

namespace App\Http\Requests\Finance;
use App\Finance;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|unique:finances|max:12',
            'date' => 'required|date',
            'quantity' => 'required|between:0,9999999.99',
            'observation' => 'required|max:10000',
            'period_id' => 'required|numeric',
            'production_id' => 'required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'code.unique' => 'El campo ya esta usado',
            'code.required' => 'El campo es requerido',
            'date.required' => 'El campo de nombre es requerido',
            'date.date' => 'El campo es incorrecto',
            'quantity.required' => 'El campo es requerido',
            'observation.required' => 'El campo es requerido',
            'period_id.required' => 'El campo es requerido',
            'period_id.numeric' => 'Incorrecto',
            'production_id.required' => 'El campo es requerido',
            'production_id.numeric' => 'Incorrecto',

        ];
    }
}
