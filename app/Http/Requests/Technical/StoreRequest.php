<?php

namespace App\Http\Requests\Technical;
use App\Technical;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|unique:technicals|max:50',
            'name' => 'required|max:10000',
            'document' => 'required',
            'technicaltype_id' => 'required|numeric',
            'production_id' => 'required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'code.required' => 'El campo es requerido',
            'code.unique' => 'El campo ya existe!',
            'code.max' => 'Código incorrecto',
            'name.required' => 'El campo de nombre es requerido',
            'document' => 'Selecione documento',
            'technicaltype_id.required' => 'El campo es requerido',
            'technicaltype_id.numeric' => 'Incorrecto',
            'production_id.required' => 'El campo es requerido',
            'production_id.numeric' => 'Incorrecto',

        ];
    }
}
