<?php

namespace App\Http\Requests\Production;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'code' => 'required|unique:productions,code,'.$this->route('production')->id.'|max:12',
            'description' => 'required|max:10000',
            'email' => 'required|min:8',
            'phone' => 'required|numeric',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'El campo de nombre es requerido',
            'code.unique' => 'El campo ya esta usado',
            'description.required' => 'El campo es requerido',
            'email.required' => 'Ingrese un correo electrónico',
            'phone.required' => 'Ingrese un número telefónico',
            'phone.numeric' => 'Ingrese un número valido',

        ];
    }
}
