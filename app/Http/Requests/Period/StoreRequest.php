<?php

namespace App\Http\Requests\Period;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'year' => 'required|numeric|min:2000|max:'.date("Y").'',
            'month' => 'required|numeric|min:1|max:12',
        ];
    }
    public function messages()
    {
        return [
            'year.required' => 'El campo año es requerido',
            'year.numeric' => 'El campo es invalido',
            'year.min' => 'Ingrese un año mayor a 2000',
            'year.max' => 'El año es incorrecto',
            'month.required' => 'El campo mes es requerido',
            'month.numeric' => 'El campo es invalido',
            'month.min' => 'El ese mes no existe',
            'month.max' => 'El ese mes no existe',
        ];
    }
}
