<?php

namespace App\Http\Controllers\Responsable;

use App\Technical;
use App\Production;
use App\Technicaltype;
use App\Technicalcondition;
use Auth;
use App\Http\Requests\Technical\StoreRequest;
use App\Http\Requests\Technicalcondition\StoreRequestcondition;

use App\Http\Requests\Technical\UpdateRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TechnicalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.pages.technical.index',[
            'technicals'=> Technical::orderBy('id','DESC')
            ->where('production_id','!=',Auth::user()->production_id)
            ->paginate(15),

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.pages.technical.create',[
            'types'=> Technicaltype::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request,Technical $technical)
    {
        $technical = $technical->store($request);
        Technicalcondition::store($technical->id,Auth::user()->id,'Se ha enviado un documento Técnico');
        return redirect()->route('user.technical.show',$technical);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Technical $technical)
    {
        return view('user.pages.technical.show',[
            'technical' => $technical,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.pages.technical.edit',[
            'types'=> Technicaltype::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Technical $technical)
    {
        $technical->my_update($request->all());
        return redirect()->route('user.technical.show',$technical);       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technical $technical)
    {
        $request->delete();
        return redirect()->route('user.technical.index'); 
    }
}
