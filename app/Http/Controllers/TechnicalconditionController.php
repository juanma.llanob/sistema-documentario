<?php

namespace App\Http\Controllers;

use App\Technicalcondition;
use App\Technical;
use Illuminate\Http\Request;
use Auth;

class TechnicalconditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Technicalcondition $technicalcondition,Technical $technical)
    {
        $technicalcondition->store($request->get('technical_id'),Auth::user()->id,$request->get('message'));

        alert('Exito','Mensaje enviado','success')->showConfirmButton();

        return redirect()->route('admin.technical.show',$technical);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Technicalcondition  $technicalcondition
     * @return \Illuminate\Http\Response
     */
    public function show(Technicalcondition $technicalcondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Technicalcondition  $technicalcondition
     * @return \Illuminate\Http\Response
     */
    public function edit(Technicalcondition $technicalcondition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Technicalcondition  $technicalcondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Technicalcondition $technicalcondition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Technicalcondition  $technicalcondition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technicalcondition $technicalcondition)
    {
        //
    }
}
