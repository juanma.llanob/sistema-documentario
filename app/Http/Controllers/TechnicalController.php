<?php

namespace App\Http\Controllers;

use App\Technical;
use App\Production;
use App\Technicaltype;
use App\Technicalcondition;
use Auth;
use App\Http\Requests\Technical\StoreRequest;
use App\Http\Requests\Technicalcondition\StoreRequestcondition;

use App\Http\Requests\Technical\UpdateRequest;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

class TechnicalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.technical.index',[
            'technicals' => Technical::orderBy('id','DESC')->paginate(15),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('admin.pages.technical.create',[
            'productions' => Production::orderBy('name')->status(1)->get(),
            'technicaltypes' => Technicaltype::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request,Technical $technical)
    {   
            $technical = $technical->store($request);
            Technicalcondition::store($technical->id,Auth::user()->id,'Se ha enviado un documento Técnico');

      
        
        return redirect()->route('admin.technical.show',$technical);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Technical  $technical
     * @return \Illuminate\Http\Response
     */
    public function show(Technical $technical)
    {
        return view('admin.pages.technical.show',[
            'technical' => $technical,
            'technicalconditions'=>$technical->technicalconditions,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Technical  $technical
     * @return \Illuminate\Http\Response
     */
    public function edit(Technical $technical)
    {
        return view('admin.pages.technical.edit',[
            'technical' => $technical,
            'productions' => Production::all(),
            'technicaltypes' => Technicaltype::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Technical  $technical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Technical $technical)
    {
        if($request->get('status')=='aprobado' || $request->get('status')=='desaprobado')
        {
            Technical::where('id',$technical->id)->update(['status'=>$request->get('status')]);
            Technicalcondition::store($technical->id,Auth::user()->id,'El documento ha sido '.$request->get('status'));
        }
        else
        {
            $technical->my_update($request);

        }    
        return redirect()->route('admin.technical.show',$technical);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Technical  $technical
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technical $technical)
    {
        $technical->delete();
        alert('Exito','Se ha eliminado un Oficio','success')->showConfirmButton();

        return redirect()->route('admin.technical.index');
    }


    public function downloadPDF(Technical $technical){

        $download=Storage::download('public/'.$technical->document,$technical->production->name.'-'.$technical->name.'.pdf');
        if($technical->status == 'enviado')
            Technical::where('id',$technical->id)->update(['status'=>'en proceso']);
        return $download;
    }



}
