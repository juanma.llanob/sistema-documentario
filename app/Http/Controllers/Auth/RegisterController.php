<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:100'],
            'surname' => ['required', 'string', 'max:100'],
            'src' => ['image'],
            'dni' => ['required', 'numeric'],
            'phone' => ['required', 'numeric'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'jobtype_id' => ['required', 'integer'],
            'production_id' => ['required', 'integer'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        // $ruta=$data['src'];
        // dd($data->hasFile('src'));
        // if ($data->hasFile('src')) 
        // {                
        //     $ruta=$request->file('src')->store('user','public');
        // }
        $user= User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'dni' => $data['dni'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'jobtype_id' => $data['jobtype_id'],
            'production_id' => $data['production_id'],
            'password' => Hash::make($data['password']),
        ]);

        $usuario = config('app.user_role');

        $role = \App\Role::where('slug','usuario' )->first();
        $permissions= $role->permissions;

        $user->roles()->attach($role);
        $user->permissions()->sync($role->permissions);

        return $user; 
    }

}
