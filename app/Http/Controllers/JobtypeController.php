<?php

namespace App\Http\Controllers;

use App\Jobtype;
use Illuminate\Http\Request;

class JobtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobtype  $jobtype
     * @return \Illuminate\Http\Response
     */
    public function show(Jobtype $jobtype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobtype  $jobtype
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobtype $jobtype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobtype  $jobtype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobtype $jobtype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobtype  $jobtype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobtype $jobtype)
    {
        //
    }
}
