<?php

namespace App\Http\Controllers;

use App\Period;
use App\Http\Requests\Period\StoreRequest;
use App\Http\Requests\Period\UpdateRequest;
use Illuminate\Http\Request;

class PeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Period::limiter_period();
        return view('admin.pages.period.index',[
            'periods' => Period::orderBy('id','DESC')->paginate(15),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.period.create');
     }   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Period $period)
    {
        Period::change_status();
        $period = $period->store($request);
        return redirect()->route('admin.period.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function show(Period $period)
    {

        return view('admin.pages.period.show',[
            'period' => $period,
            'finances' =>  $period->finances,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function edit(Period $period)
    {
        return view('admin.pages.period.edit',[
            'period' => $period,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Period $period)
    {
        Period::change_status();
        $period->my_update($request);
        return redirect()->route('admin.period.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function destroy(Period $period)
    {
        //
    }


}





