<?php

namespace App\Http\Controllers;

use App\Technicaltype;
use Illuminate\Http\Request;

class TechnicaltypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Technicaltype  $technicaltype
     * @return \Illuminate\Http\Response
     */
    public function show(Technicaltype $technicaltype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Technicaltype  $technicaltype
     * @return \Illuminate\Http\Response
     */
    public function edit(Technicaltype $technicaltype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Technicaltype  $technicaltype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Technicaltype $technicaltype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Technicaltype  $technicaltype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technicaltype $technicaltype)
    {
        //
    }
}
