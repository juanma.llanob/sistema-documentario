<?php

namespace App\Http\Controllers;
use App\User;
use App\Production;
use App\Jobtype;
use App\Role;
use App\Permission;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.pages.user.index',[
            'users' => User::orderBy('id','DESC')->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.user.create',[
            'productions' => Production::all(),
            'jobtypes' => Jobtype::all(),
            'roles'=> Role::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, User $user)
    {
        $user=$user->store($request);
        return redirect()->route('admin.user.show',$user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        return view('admin.pages.user.show',[
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.pages.user.edit',[
            'user' => $user,
            'productions' => Production::all(),
            'jobtypes' => Jobtype::all(),
            'roles' => Role::all(),
            'role_permissions' => $user->roles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, User $user)
    {
        $user->my_update($request);
 
        return redirect()->route('admin.user.show',$user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->update(['status' => 0]);
        return back();
    }


    // Desbloquear Usuario
    public function update_all( User $user)
    {
        $user->where('id',$user->id)->update(['status' => 1]);
 
        return redirect()->route('admin.user.show',$user);
    }

    
    // Mostar formulario para asignar rol
    public function assign_role(User $user){

        return view('admin.pages.user.assign_role',[
            'user' => $user,
            'roles' => Role::all(),
        ]);

    }
    // Asignar los roles en la base de datos pivote
    public function role_assignment(Request $request, User $user){
        $user->roles()->sync($request->roles);
        return back();
    }
    //Asignar los permisos
    public function assign_permission(User $user){

        return view('admin.pages.user.assign_permission',[
            'user' => $user,
            'roles' => $user->roles,
        ]);

    }
    // Asignar los permisos en la base de datos pivote
    public function permission_assignment(Request $request, User $user){
        $user->permissions()->sync($request->permissions);
        return back();
    }

}
