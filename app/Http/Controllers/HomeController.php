<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Technical;
use App\Event;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.admin');
    }
    public function user_index(){
        return view('user.home',[
            'technicals' => Technical::orderBy('id','DESC')
            ->where('production_id','!=',Auth::user()->production_id)
            ->paginate(5),
            'events'=> Event::orderBy('id','DESC')
            ->where('status',1)->paginate(10),
        ]);
    }

}
