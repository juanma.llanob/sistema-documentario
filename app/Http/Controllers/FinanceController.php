<?php

namespace App\Http\Controllers;

use App\Finance;
use App\Production;
use App\Period;
use App\Http\Requests\Finance\StoreRequest;
use App\Http\Requests\Finance\UpdateRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\FinancesImport;

class FinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$prueba=Production::select('id','name')->join('finances','id','=','id')->distinct();
        //dd($prueba);

        return view('admin.pages.finance.index',[
            'productions' => Production::orderBy('id','DESC')
            ->status(1)
            ->paginate(15),
            'period'=> Period::current_period(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.pages.finance.create',[
            'periods' => Period::all(),
            'productions' => Production::all(),
            'period' => Period::current_period(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Finance $finance)
    {
        $finance = $finance->store($request);
        return redirect()->route('admin.finance.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function show(Finance $finance)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function edit(Finance $finance)
    {
        return view('admin.pages.finance.edit',[
            'finance' => $finance,
            'periods' => Period::all(),
            'productions' => Production::all(),
            'period' => Period::current_period(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Finance $finance)
    {
        $finance->update($request->all());
        return redirect()->route('admin.finance.show_detail',$finance->production);
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Finance $finance)
    {   
        $aux=$finance;
        $finance->delete();
        return redirect()->route('admin.finance.show_detail',$aux->production);
    }

    //Otros Metodos
    public function show_detail(Production $production)
    {   
        $period= Period::current_period();
        $filtrar=['period_id'=>$period->id,'production_id'=>$production->id];
        return view('admin.pages.finance.show',[
            'production' => $production,
            'finances' => Finance::where($filtrar)->orderBy('id','DESC')->paginate(),
            'finance' => Period::current_period(),
            'total' => Finance::production_month_total(Period::current_period(),$production->id),
        ]);
    }
    public function upload_excel(Request $request){
        $file = $request->file('file');
        Excel::import(new FinancesImport, $file);
        return back();
    }

}
