<?php

namespace App\Http\Controllers;

use App\Production;
use App\User;
use Auth;
use App\Http\Requests\Production\UpdateRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Production\StoreRequest;
class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.production.index',[
            'productions' => Production::orderBy('id','DESC')->paginate(),
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.production.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request,Production $production)
    {
        $production = $production->store($request);
        return redirect()->route('admin.production.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function show(Production $production)
    {

        return view('admin.pages.production.show',[
            'production' => $production,
            'users' => $production->users,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function edit(Production $production)
    {
        return view('admin.pages.production.edit',[
            'production' => $production,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Production $production)
    {
        
        $production->my_update($request);
        return redirect()->route('admin.production.show',$production);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function destroy(Production $production)
    {   
        
        $user = User::where('production_id',$production->id)->update(['status'=>0]);
        Production::where('id',$production->id)->update(['status'=>0]);
        return redirect()->route('admin.production.show',$production);
    }

    public function update_all(Production $production)
    {
        
        $delete_user = User::where('production_id',$production->id)->update(['status'=>1]);
        Production::where('id',$production->id)->update(['status'=>1]);



        return redirect()->route('admin.production.show',$production);
    }



}
