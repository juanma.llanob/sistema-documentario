<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/prueba',function(){
	return view('user.home');
}); 

Route::get('/usuario',function(){
	return view('user.pages.user.show');
}); 
Route::get('/centro',function(){
	return view('user.pages.production.show');
}); 

Route::get('/editarc',function(){
	return view('user.pages.production.edit');
}); 

Route::get('/fc',function(){
	return view('user.pages.finance.create');
}); 

Route::get('/fe',function(){
	return view('user.pages.finance.edit');
}); 

Route::get('/fi',function(){
	return view('user.pages.finance.index');
}); 

Route::get('/fsm',function(){
	return view('user.pages.finance.showmonth');
}); 
Route::get('/fsy',function(){
	return view('user.pages.finance.showyear');
}); 


Route::get('/tc',function(){
	return view('user.pages.technical.create');
}); 

Route::get('/te',function(){
	return view('user.pages.technical.edit');
}); 

Route::get('/ti',function(){
	return view('user.pages.technical.index');
}); 

Route::get('/ts',function(){
	return view('user.pages.technical.show');
}); 





Route::get('/',function(){
	return view('auth.login');
}); 

Auth::routes(['verify'=>true]);

Route::group(['middleware'=>['auth','user:Encargado-Administrador'],'as' => 'user.'], function(){
	Route::get('/','HomeController@user_index')->name('inicio');

	Route::resource('responsable/finance','Responsable\FinanceController');
	Route::resource('responsable/production','Responsable\ProductionController');
	Route::resource('responsable/user','Responsable\UserController');
	Route::resource('responsable/technical','Responsable\TechnicalController');
	Route::post('responsable/upload_excel','Responsable\FinanceController@upload_excel')->name('finance.upload_excel');
	Route::post('responsable/technical/{technical}','Responsable\TechnicalconditionController@store')->name('technicalcondition.store');



});


Route::group(['middleware' => ['auth','user:Administrador'], 'as' => 'admin.'], function(){
	Route::get('/home','HomeController@index')->name('home');

	//Producione
	Route::resource('production','ProductionController');
	Route::get('production/{production}/update_all','ProductionController@update_all')->name('production.update_all');
	//Finance
	Route::get('finance/{production}/show_detail','FinanceController@show_detail')->name('finance.show_detail');
	Route::post('finance/upload_excel','FinanceController@upload_excel')->name('finance.upload_excel');
	Route::resource('finance','FinanceController');


	Route::resource('technical','TechnicalController');
	Route::get('technical/{technical}/downloadPDF','TechnicalController@downloadPDF')->name('technical.downloadPDF');

	Route::resource('technicalcondition','TechnicalconditionController');
	Route::post('technical/{technical}','TechnicalconditionController@store')->name('technicalcondition.store');


	Route::resource('event','EventController');
	//Period
	Route::resource('period','PeriodController');
	//user
	Route::resource('user','UserController');
	Route::get('user/{user}/assign_role','UserController@assign_role')->name('user.assign_role');
	Route::post('user/{user}/role_assignment','UserController@role_assignment')->name('user.role_assignment');
	Route::get('user/{user}/assign_permission','UserController@assign_permission')->name('user.assign_permission');
	Route::post('user/{user}/permission_assignment','UserController@permission_assignment')->name('user.permission_assignment');
	Route::post('user/{user}/update_all','UserController@update_all')->name('user.update_all');


	Route::resource('role','RoleController'); 
	Route::resource('permission','PermissionController');
});



Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});








